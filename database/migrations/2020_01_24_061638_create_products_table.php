<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('pro_id');
            $table->string('pro_code');
            $table->string('pro_name');
            $table->integer('pro_category_id');
            $table->text('pro_image');
            $table->decimal('pro_price', 20, 0);
            $table->string('pro_brand');
            $table->string('pro_discount')->nullable();
            $table->string('pro_quantity');
            $table->string('pro_wattage');
            $table->string('pro_height_weight');
            $table->text('pro_description');
            $table->integer('pro_supplier_id')->nullable();
            $table->integer('pro_status')->nullable()->default(1);
            $table->integer('pro_active')->default(1);
            $table->text('thumbnail_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
