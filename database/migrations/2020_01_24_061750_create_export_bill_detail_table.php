<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportBillDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('export_bill_detail', function (Blueprint $table) {
            $table->bigIncrements('edetail_id');
            $table->integer('edetail_product_id');
            $table->integer('edetail_quantity');
            $table->integer('edetail_unit_price');
            $table->integer('edetail_total_money');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('export_bill_detail');
    }
}
