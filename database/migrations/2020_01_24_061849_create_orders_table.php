<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('or_id');
            $table->string('or_cus_name');
            $table->string('or_cus_email');
            $table->integer('or_cus_phone');
            $table->string('or_cus_city');
            $table->string('or_cus_address');
            $table->date('or_shipped_date');
            $table->integer('or_pay_id');
            $table->integer('or_user_id');
            $table->text('or_notes');
            $table->bigInteger('or_total');
            $table->integer('or_status_id');
            $table->integer('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
