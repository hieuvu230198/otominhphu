<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportBillDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_bill_detail', function (Blueprint $table) {
            $table->bigIncrements('idetail_id');
            $table->integer('idetail_product_id');
            $table->integer('idetail_quantity');
            $table->integer('idetail_unit_price');
            $table->integer('idetail_total_money');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_bill_detail');
    }
}
