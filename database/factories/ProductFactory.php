<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'pro_code' => $faker->randomNumber,
        'pro_name' => $faker->name,
        'pro_category_id' => $faker->numberBetween(1,5),
        'pro_image' => $faker->imageUrl($width = 640, $height = 480),
        'pro_price' => $faker->randomNumber,
        'pro_brand' => $faker->name,
        'pro_discount' => $faker->numberBetween(1,10),
        'pro_quantity' => $faker->randomNumber,
        'pro_description' => $faker->paragraph,
        'pro_supplier_id' => $faker->numberBetween(1,5),
        'pro_status' => 1,
        'pro_active' => 1,
        'created_at' => new DateTime,
        'updated_at' => new DateTime,
    ];
});

