<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Supplier;
use Faker\Generator as Faker;

$factory->define(Supplier::class, function (Faker $faker) {
    return [
        'supp_name' => $faker->word,
        'supp_address' => $faker->word,
        'supp_phone' => $faker->randomNumber,
        'supp_email' => $faker->word,
        'supp_logo' => $faker->word,
    ];
});
