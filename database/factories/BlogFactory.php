<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blog;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'blog_image' => $faker->word,
        'blog_title' => $faker->sentence,
        'blog_description' => $faker->text,
        'blog_content'  => $faker->paragraph,
        'blog_user_id' => 1
    ];
});
