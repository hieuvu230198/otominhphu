<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        factory(\App\Blog::class, 10)->create();
        factory(\App\Slider::class, 5)->create();
        factory(\App\Product::class, 20)->create();
        factory(\App\Supplier::class, 10)->create();
        DB::table('roles')->insert([
            ['name' => 'admin', 'display_name' => 'Admin'],
            ['name' => 'user', 'display_name' => 'User'],
        ]);
    }
}
