<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=> 'Root',
            'email' => 'admin@gmail.com',
            'username' => 'root',
            'email_verified_at' => now(),
            'password' => bcrypt('password'), // password
            'phone' => '0344125660',
            'dateofbirth' => now(),
            'department' => 'Development Manager',
            'role_id' => 1,
            'remember_token' => Str::random(10),
        ]);
        User::create([
            'name'=> 'User',
            'email' => 'user1@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('password'), // password
            'phone' => '0344125660',
            'dateofbirth' => now(),
            'department' => 'Development Manager',
            'role_id' => 2,
            'remember_token' => Str::random(10),
        ]);
        factory(User::class, 30)->create();
    }
}
