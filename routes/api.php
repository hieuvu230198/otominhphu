<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=> 'v1'], function () {
    // ======================= BACKEND =====================
    Route::group(['prefix' => 'auth'], function () {
        // Login User
        Route::post('login', "api\backend\AuthController@login")->name('login');
        // Refresh the JWT Token
        Route::get('refresh', "api\backend\AuthController@refresh");
        // Logout user from application
        Route::post('logout', "api\backend\AuthController@logout");
        Route::post('me', "api\backend\AuthController@getMe");
    });
    Route::group(['prefix' => 'home', 'middleware' => 'jwt.auth'], function () {
         Route::get('/get-data-dashboard', "api\backend\DashboardController@getDataDashBoard");
    });
    Route::group(['prefix' => 'user', 'middleware' => 'jwt.auth'], function () {
        Route::get('/get-all-users', "api\backend\UserController@getAllUsers");
        Route::get('/get-user-with-id/{userId}', "api\backend\UserController@getUserWithId");
        Route::post('/create', "api\backend\UserController@store");
        Route::post('/update/{userId}', "api\backend\UserController@update");
        Route::put('/{userId}/change-password', "api\backend\UserController@changePassword");
        Route::delete('/delete/{userId}', "api\backend\UserController@deleteUser");
        Route::post('/search', "api\backend\UserController@search");
    });
    Route::group(['prefix' => 'category', 'middleware' => 'jwt.auth'], function () {
        Route::get('/get-all-categories', "api\backend\CategoryController@getAllCategories");
        Route::post('/create', "api\backend\CategoryController@store");
        Route::post('/update/{categoryId}', "api\backend\CategoryController@update");
        Route::delete('/delete/{categoryId}', "api\backend\CategoryController@deleteCategory");
        Route::post('/search', "api\backend\CategoryController@search");
    });
    Route::group(['prefix' => 'bill', 'middleware' => 'jwt.auth'], function () {
        Route::get('/get-all-bills', "api\backend\BillController@getAllBills");
        Route::post('/change-status-id', "api\backend\BillController@changeStatus");
        Route::get('/get-bill-with-id/{billId}', "api\backend\BillController@getBillDetail");
        Route::delete('/delete/{billId}', "api\backend\BillController@deleteBill");
        Route::post('/search', "api\backend\BillController@search");
        Route::post('/send-mail', "api\backend\BillController@sendMail");
    });
    Route::group(['prefix' => 'blog', 'middleware' => 'jwt.auth'], function () {
        Route::get('/get-all-blogs', "api\backend\BlogController@getAllBlogs");
        Route::delete('/delete/{blogId}', "api\backend\BlogController@deleteBlog");
        Route::post('/create', "api\backend\BlogController@store");
        Route::post('/update/{blogId}', "api\backend\BlogController@update");
        Route::post('/search', "api\backend\BlogController@search");
    });
    Route::group(['prefix' => 'slider', 'middleware' => 'jwt.auth'], function () {
        Route::get('/get-all-sliders', "api\backend\SliderController@getAllSliders");
        Route::delete('/delete/{sliderId}', "api\backend\SliderController@deleteSlider");
        Route::post('/create', "api\backend\SliderController@store");
        Route::post('/update/{sliderId}', "api\backend\SliderController@update");
        Route::post('/search', "api\backend\SliderController@search");
    });
    Route::group(['prefix' => 'product', 'middleware' => 'jwt.auth'], function () {
        Route::get('/get-all-products', "api\backend\ProductController@getAllProducts");
        Route::delete('/delete/{productId}', "api\backend\ProductController@deleteProduct");
        Route::post('/create', "api\backend\ProductController@store");
        Route::post('/update/{productId}', "api\backend\ProductController@update");
        Route::post('/search', "api\backend\ProductController@search");
    });
    Route::group(['prefix' => 'supplier', 'middleware' => 'jwt.auth'], function () {
        Route::get('/get-all-suppliers', "api\backend\SupplierController@getAllSuppliers");
        Route::delete('/delete/{supplierId}', "api\backend\SupplierController@deleteSupplier");
        Route::post('/create', "api\backend\SupplierController@store");
        Route::post('/update/{supplierId}', "api\backend\SupplierController@update");
        Route::post('/search', "api\backend\SupplierController@search");
    });

    // ======================= FRONTEND =====================
    Route::get('/get-all-categories', 'api\frontend\CategoryController@getAllCategories');
    Route::get('/get-all-blogs/{offset}', 'api\frontend\BlogController@getAllBlogs');
    Route::get('/get-detail-blog/{offset}', 'api\frontend\BlogController@getBlogDetail');
    Route::post('/post-cart', 'api\frontend\CartController@postCart');
    // Product
    Route::get('/get-old-products/{offset}', 'api\frontend\ProductController@getOldProducts');
    Route::get('/get-all-products/{offset}', 'api\frontend\ProductController@getAllProducts');
    Route::get('/get-new-detail-products', 'api\frontend\ProductController@getNewDetailProducts');
    Route::get('/get-related-product/{category_id}', 'api\frontend\ProductController@getRelatedProducts');
    Route::get('/get-product-by-category/{categoryId}', 'api\frontend\ProductController@getProductByCategory');
    Route::get('/get-new-products', 'api\frontend\ProductController@getNewProducts');
    Route::get('/search', 'api\frontend\ProductController@searchData');
});


