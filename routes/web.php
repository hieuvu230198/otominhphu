<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/admin/{any}', "backend\HomeController@index")->where('any','.*');
Route::get('/admin', "backend\HomeController@index")->where('any','.*');
// ======================= FRONTEND =====================
Route::get('/', "frontend\HomeController@index")->where('any','.*');
Route::get('/tin-tuc', "frontend\BlogController@index")->where('any','.*');
Route::get('/shopping', "frontend\ShoppingController@index")->where('any','.*');
Route::get('/tin-tuc/blog-detail', "frontend\BlogController@detailBlog")->where('any','.*');
Route::get('/cart-view', "frontend\CartController@index")->where('any','.*');
Route::get('/search', 'frontend\HomeController@searchProduct')->where('any','.*');

Route::get('/lien-he', "frontend\ContactController@index")->where('any','.*');
Route::get('/ve-chung-toi', "frontend\AboutUsController@index")->where('any','.*');

Route::get('/san-pham/{productId}', "frontend\ShoppingController@productDetail");
Route::get('/danh-sach-san-pham/{categoryId}', "frontend\ShoppingController@productByCategory");

Route::get('bill/download','backend\DownLoadBillController@index');
