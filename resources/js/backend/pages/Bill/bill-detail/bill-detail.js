import BillService from "../../../services/bill.service";
import {alertResponseError, alertResponseSuccess} from "../../../helpers/alertMessage";
import {messages} from "../../../common/message";

export default {
    data() {
        return {
            bill : {},
            billId: '',
            billDetail: {},
            total: 0,
        }
    },
    methods: {
        getAllBillDetail(billId) {
            return BillService.getBillWithId(billId).then((response) => {
                if (response.data.success) {
                    this.billDetail = response.data.data.billDetail;
                    this.bill = response.data.data.bill;
                    this.total = response.data.data.bill.or_total;
                }
            }).catch((error) => {
                return alertResponseError(error.response.message);
            });
        },
        updateStatusBill(billId, status) {
            return BillService.updateStatusBill({billId: billId, status: status}).then((response) => {
                if (response.data.success) {
                    alertResponseSuccess(messages.UPDATE_SUCCESS);
                    this.getAllBillDetail(this.billId);
                }
            }).catch((error) => {
                return alertResponseError(error.response.message);
            })
        },
        sendMail() {
            const data = {
                customer: this.bill,
                billDetail: this.billDetail,
            };
            return BillService.sendMail(data).then((response) => {
                if (response) {
                    alertResponseSuccess('Gửi mail thành công!');
                }
            }).catch((error) => {
                console.log(error);
            })
        },
        printBill() {
            window.print();
        }
    },
    created() {
        const id = this.$route.params.billId;
        this.billId = id;
        this.getAllBillDetail(id);
    },
}
