import BillService from "../../../services/bill.service";
import {alertResponseError, alertResponseSuccess} from "../../../helpers/alertMessage";
import {checkRoleAdmin} from "../../../helpers/checkRole";


export default {
    mixins: [checkRoleAdmin],
    data() {
        return {
            isAdmin: false,
            arrBillDetail:[],
            billDetail:[],
            arrBills: [],
            arrProducts: [],
            pagination : {
                currentPage: 1,
                pageCount: 1,
            },
            formSubmitted: false,
            editmode: false,
            bill:{
                or_id:'',
                or_cus_name : '',
                or_cus_email: '',
                or_cus_phone: '',
                or_cus_city: '',
                or_cus_address: '',
                or_shipped_date: '',
                or_pay_id: '',
                or_notes: '',
                or_status_id: '',
            },
            dataSearch:'',
        }
    },
    methods: {
        getAllBills() {
            return BillService.getAllBills(this.pagination.currentPage).then(response => {
                this.arrBills = response.data.data.data;
                this.pagination.currentPage = response.data.data.current_page;
                this.pagination.pageCount = response.data.data.last_page;
            }).catch((error) => {
                return alertResponseError(error.response.data.message);
            });
        },
        deleteBill(billId) {
            Swal.fire({
                text: 'Bạn có muốn xóa hóa đơn?',
                icon: 'info',
                showCloseButton: true,
                showCancelButton: true,
                showConfirmButton: true,
            }).then((result) => {
                if (result.value) {
                    return BillService.deleteBill(billId).then((response) => {
                        if (response.data.success) {
                            alertResponseSuccess('Xóa thành công!').then((value) => {
                                if(value) {
                                    return this.getAllBills();
                                }
                            });
                        }
                    }).catch((error) => {
                        return alertResponseError(error.response);
                    });
                }
            });
        },
        searchData() {
            if (this.dataSearch) {
                return BillService.searchBills({key: this.dataSearch}).then((response) => {
                    this.arrBills = response.data.data.data;
                    this.pagination.currentPage = response.data.data.current_page;
                    this.pagination.pageCount = response.data.data.last_page;
                }).catch((error) => {
                    return alertResponseError(error.response.data.message);
                })
            }
        },
        newModal(){
            this.editmode = false;
            this.bill = {};
            $('#addNew').modal('show');
        },

    },
    created() {
        this.getAllBills();
        const loginUser = JSON.parse(localStorage.getItem('user'));
        this.isAdmin = this.checkRoleAdmin(loginUser);
        this.reloadData();
    },
}
