import SliderService from "../../../services/slider.service";
import {alertResponseError, alertResponseSuccess} from "../../../helpers/alertMessage";

export default {
    data() {
        return {
            arrSliders: [],
            pagination : {
                currentPage: 1,
                pageCount: 1,
            },
            formSubmitted: false,
            editmode: false,
            slider: {
                sli_id:'',
                sli_name : '',
                sli_image : '',
                thumbnail_path:'',
            },
            dataSearch:'',
        }
    },
    methods: {
        searchData() {
            if (this.dataSearch) {
                return SliderService.searchSlider({key: this.dataSearch}).then((response) => {
                    this.arrSliders = response.data.data.data;
                    this.pagination.currentPage = response.data.data.current_page;
                    this.pagination.pageCount = response.data.data.last_page;
                }).catch((error) => {
                    alertResponseError(error);
                })
            }
        },
        getAllSliders() {
            SliderService.getAllSliders(this.pagination.currentPage).then(response => {
                this.arrSliders = response.data.data.data;
                this.pagination.currentPage = response.data.data.current_page;
                this.pagination.pageCount = response.data.data.last_page;
            }).catch((error) => {
                alertResponseError(error);
            });
        },

        deleteSlider(sliderId) {
            Swal.fire({
                text: 'Xóa Slider này?',
                icon: 'info',
                showCloseButton: true,
                showCancelButton: true,
                showConfirmButton: true,
            }).then((result) => {
                if (result.value) {
                    SliderService.deleteSlider(sliderId).then((response) => {
                        if (response.data.success) {
                            alertResponseSuccess('Xóa thành công!').then((value) => {
                                if(value) {
                                    return this.getAllSliders();
                                }
                            });
                        }
                    }).catch((error) => {
                        return alertResponseError(error.response.data.message);
                    });
                }
            });
        },

        handleForm() {
            this.formSubmitted = true;
            if (this.editmode) {
                this.updateSlider();
            } else {
                this.createSlider();
            }
        },
        createSlider(){
            const data = new FormData(this.$refs.formSlider);
            SliderService.createSlider(data).then((response) => {
                this.formSubmitted = false;
                this.$refs.formSlider.value = null;
                if (response.data.success) {
                    $('#addNew').modal('hide');
                    alertResponseSuccess('Thêm mới thành công!').then((value) => {
                        if(value) {
                            return this.getAllSliders();
                        }
                    });
                }
            }).catch((error) => {
                alertResponseError(error);
            })
        },

        updateSlider() {
            const data = new FormData(this.$refs.formSlider);
            SliderService.updateSlider(this.slider.sli_id, data).then((response) => {
                this.formSubmitted = false;
                if (response.data.success) {
                    $('#addNew').modal('hide');
                    alertResponseSuccess('Sửa thành công!').then((value) => {
                        if(value) {
                            return this.getAllSliders();
                        }
                    });
                }
            }).catch((error) => {
                alertResponseError(error);
            });
        },

        editModal(slider){
            this.slider = {};
            this.editmode = true;
            this.slider = slider;
            $('#addNew').modal('show');
        },
        newModal() {
            this.$refs.image_slider.value = null;
            this.editmode = false;
            this.slider = {};
            $('#addNew').modal('show');
        },
    },
    created() {
        this.getAllSliders();
    },
}
