import { AuthService } from "../../../services/auth.service";
import { required, email } from "vuelidate/lib/validators";
import { messages } from "../../../common/message";
import {alertResponseError} from "../../../helpers/alertMessage";

export default {
    data() {
        return {
            error: '',
            user: {
                email: '',
                password: '',
            },
            formSubmitted: false
        }
    },
    validations: {
        user: {
            email: { required, email },
            password: { required }
        }
    },
    methods: {
        handleLogin() {
            this.formSubmitted = true;
            this.$v.$touch();
            if (this.$v.$invalid) {
                return false;
            } else {
                this.submitForm();
            }
        },
        submitForm() {
            AuthService.login(this.user).then(response => {
                this.formSubmitted = false;
                if(response.data.success) {
                    AuthService.saveToken(response.data.data.access_token);
                    AuthService.saveUserCurrentLogin(response.data.data.user);
                    this.$router.push({name: 'admin-dashboard'});
                }
            }).catch((error) => {
                this.error = messages.WRONG_PASSWORD_OR_EMAIL;
                if (error.response.data) {
                    alertResponseError(error.response.data.message);
                }
            });
        }
    },
    computed: {
        emailErrorMsg(){
            if(!this.$v.user.email.required){
                return messages.EMAIl_REQUIRED;
            }
            if(!this.$v.user.email.email){
                return messages.EMAIL_INVALID;
            }
            if(this.errors && this.errors.email){
                return this.errors.email[0];
            }
            return null;
        },
        passwordError(){
            if(!this.$v.user.password.required){
                return messages.PASSWORD_REQUIRED;
            }
            if(this.errors && this.errors.password){
                return this.errors.password;
            }
            return null;
        },
    }
}
