import DashBoardService from "../../services/dashboard.service";
export default {
    data() {
        return {
            countUser: 0,
            countProduct: 0,
            countSuppliers: 0,
            userPerfect: "",
            topCustomer: {},
        };
    },
    methods: {
        getAllDashBoard() {
            DashBoardService.getDataDashBoard()
                .then(response => {
                    if (response.data.success) {
                        this.countUser = response.data.data.countUser;
                        this.countProduct = response.data.data.countProduct;
                        this.userPerfect = response.data.data.userPerfect;
                        this.countSuppliers = response.data.data.countSuppliers;
                        this.topCustomer = response.data.data.topCustomer;
                    }
                })
                .catch(response => {
                    this.handleError(response);
                });
        }
    },
    created() {
        this.getAllDashBoard();
    }
};
