import SupplierService from "../../services/supplier.service";
import {alertResponseError, alertResponseSuccess} from "../../helpers/alertMessage";

export default {
    data() {
        return {
            arrSuppliers: [],
            formSubmitted: false,
            editmode: false,
            pagination:{
                currentPage: 1,
                pageCount: 1,
            },
            supplier:{
                supp_id:'',
                supp_name : '',
                supp_address: '',
                supp_phone: '',
                supp_email: '',
                supp_logo: '',
                thumbnail_path: '',
            },
            dataSearch:'',
        }
    },
    methods: {
        searchData() {
            if (this.dataSearch) {
                return SupplierService.searchSupplier({key: this.dataSearch}).then((response) => {
                    this.arrSuppliers = response.data.data.data;
                    this.pagination.currentPage = response.data.data.current_page;
                    this.pagination.pageCount = response.data.data.last_page;
                }).catch((error) => {
                    alertResponseError(error);
                })
            }
        },
        getAllSuppliers() {
            SupplierService.getAllSuppliers().then(response => {
                this.arrSuppliers = response.data.data;
            }).catch((error) => {
                alertResponseError(error);
            });
        },
        deleteSupplier(supplierId) {
            Swal.fire({
                text: 'Xóa nhà cung cấp này?',
                icon: 'info',
                showCloseButton: true,
                showCancelButton: true,
                showConfirmButton: true,
            }).then((result) => {
                if (result.value) {
                    SupplierService.deleteSupplier(supplierId).then((response) => {
                        if (response.data.success) {
                            alertResponseSuccess('Xóa thành công!').then((value) => {
                                if(value) {
                                    return this.getAllSuppliers();
                                }
                            });
                        }
                    }).catch((error) => {
                        return alertResponseError(error.response.data.message);
                    });
                }
            });
        },
        handleForm() {
            this.formSubmitted = true;
            if (this.editmode) {
                this.updateSupplier();
            } else {
                this.createSupplier();
            }
        },
        createSupplier(){
            const data = new FormData(this.$refs.formSupplier);
            SupplierService.createSupplier(data).then((response) => {
                this.formSubmitted = false;
                this.$refs.formSupplier.value = null;
                $('#addNew').modal('hide');
                if (response.data.success) {
                    alertResponseSuccess('Thêm mới thành công!').then((value) => {
                        if(value) {
                            return this.getAllSuppliers();
                        }
                    });
                }
            }).catch((error) => {
                alertResponseError(error);
            })
        },
        updateSupplier() {
            const data = new FormData(this.$refs.formSupplier);
            SupplierService.updateSupplier(this.supplier.supp_id, data).then((response) => {
                this.formSubmitted = false;
                this.$refs.formSupplier.value = null;
                if (response.data.success) {
                    $('#addNew').modal('hide');
                    alertResponseSuccess('Cập nhật thành công!').then((value) => {
                        if(value) {
                            return this.getAllSuppliers();
                        }
                    });
                }
            }).catch((error) => {
                alertResponseError(error);
            });
        },
        editModal(supplier) {
            this.supplier = {};
            this.editmode = true;
            this.supplier = supplier;
            $('#addNew').modal('show');
        },
        newModal() {
            this.editmode = false;
            this.supplier = {};
            $('#addNew').modal('show');
        },
    },
    created() {
        this.getAllSuppliers();
    },
}

