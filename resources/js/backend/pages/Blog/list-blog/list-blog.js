import BlogService from "../../../services/blog.service";
import {alertResponseError, alertResponseSuccess} from "../../../helpers/alertMessage";

export default {
    data() {
        return {
            arrBlogs: [],
            arrUsers: [],
            pagination:{
                currentPage: 1,
                pageCount: 1,
            },
            formSubmitted: false,
            editmode: false,
            blog: {
                blog_id:'',
                blog_title : '',
                blog_description: '',
                blog_content: '',
                blog_user_id: '',
                blog_image: '',
                thumbnail_path: '',
            },
            dataSearch:'',
        }
    },
    methods: {
        searchData() {
            if (this.dataSearch) {
                return BlogService.searchSlider({key: this.dataSearch}).then((response) => {
                    this.arrBlogs = response.data.data.data;
                    this.pagination.currentPage = response.data.data.current_page;
                    this.pagination.pageCount = response.data.data.last_page;
                }).catch((error) => {
                    alertResponseError(error);
                })
            }
        },
        getAllBlogs() {
            BlogService.getAllBlogs(this.pagination.currentPage).then(response => {
                this.arrBlogs = response.data.data.data;
                this.pagination.currentPage = response.data.data.current_page;
                this.pagination.pageCount = response.data.data.last_page;
            }).catch((error) => {
                alertResponseError(error);
            });
        },
        deleteBlog(blogId) {
            Swal.fire({
                text: 'Xóa tin tức này?',
                icon: 'info',
                showCloseButton: true,
                showCancelButton: true,
                showConfirmButton: true,
            }).then((result) => {
                if (result.value) {
                    BlogService.deleteBlog(blogId).then((response) => {
                        if (response.data.success) {
                            alertResponseSuccess('Xóa tin tức thành công!').then((value) => {
                                if(value) {
                                    return this.getAllBlogs();
                                }
                            });
                        }
                    }).catch((error) => {
                        return alertResponseError(error.response.data.message);
                    });
                }
            });
        },
        handleForm() {
            this.formSubmitted = true;
            if (this.editmode) {
                this.updateBlog();
            } else {
                this.createBlog();
            }
        },
        createBlog(){
            const data = new FormData(this.$refs.formBlog);
            BlogService.createBlog(data).then((response) => {
                this.formSubmitted = false;
                this.$refs.formBlog.value = null;
                if (response.data.success) {
                    $('#addNew').modal('hide');
                    alertResponseSuccess('Thêm mới thành công!').then((value) => {
                        if(value) {
                            return this.getAllBlogs();
                        }
                    });
                }
            }).catch((error) => {
                alertResponseError(error);
            })
        },
        updateBlog() {
            const data = new FormData(this.$refs.formBlog);
            BlogService.updateBlog(this.blog.blog_id, data).then((response) => {
                this.formSubmitted = false;
                if (response.data.success) {
                    $('#addNew').modal('hide');
                    alertResponseSuccess('Cập nhật thành công!').then((value) => {
                        if(value) {
                            return this.getAllBlogs();
                        }
                    });
                }
            }).catch((error) => {
                alertResponseError(error);
            });
        },
        editModal(blog){
            this.blog = {};
            this.editmode = true;
            this.blog = blog;
            $('#addNew').modal('show');
        },
        newModal() {
            this.$refs.image_blog.value = null;
            this.editmode = false;
            this.blog = {};
            $('#addNew').modal('show');
        },
    },
    created() {
        this.getAllBlogs();
    },
}
