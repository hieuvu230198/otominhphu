import CategoryService from "../../services/category.service";
import { alertResponseError, alertResponseSuccess } from "../../helpers/alertMessage";
import { required } from "vuelidate/lib/validators";
import BlogService from "../../services/blog.service";
export default {
    data() {
        return {
            arrCategories: [],
            formSubmitted: false,
            editmode: false,
            category:{
                cate_id:'',
                name : '',
                description: '',
            },
            dataSearch:'',
        }
    },
    methods: {
        searchData() {
            if (this.dataSearch) {
                return CategoryService.searchCategory({key: this.dataSearch}).then((response) => {
                    if(response.data.success) {
                        this.arrCategories = response.data.data;
                    }
                }).catch((error) => {
                    alertResponseError(error);
                })
            }
        },
        getAllCategories() {
            CategoryService.getAllCategories().then(response => {
                if(response.data.success) {
                    this.arrCategories = response.data.data;
                }
            }).catch((response) => {
                this.handleError(response);
            });
        },
        deleteCategory(categoryId) {
            Swal.fire({
                text: 'Xóa loại sản phẩm này?',
                icon: 'info',
                showCloseButton: true,
                showCancelButton: true,
                showConfirmButton: true,
            }).then((result) => {
                if (result.value) {
                    CategoryService.deleteCategory(categoryId).then((response) => {
                        if (response.data.success) {
                            alertResponseSuccess('Xóa thành công!').then((value) => {
                                if(value) {
                                    return this.getAllCategories();
                                }
                            });
                        }
                    }).catch((error) => {
                        return alertResponseError(error.response.data.message);
                    });
                }
            });
        },
        handleForm() {
            this.formSubmitted = true;
            if (this.editmode) {
                this.updateCategory();
            } else {
                this.createCategory();
            }
        },
        createCategory(){
            const data = new FormData(this.$refs.formCategory);
            CategoryService.createCategory(data).then((response) => {
                this.formSubmitted = false;
                this.$refs.formCategory.value = null;
                if (response.data.success) {
                    $('#addNew').modal('hide');
                    alertResponseSuccess('Thêm mới thành công!').then((value) => {
                        if(value) {
                            return this.getAllCategories();
                        }
                    });
                }
            }).catch((error) => {
                alertResponseError(error);
            })
        },
        updateCategory() {
            const data = new FormData(this.$refs.formCategory);
            CategoryService.updateCategory(this.category.cate_id, data).then((response) => {
                this.formSubmitted = false;
                if (response.data.success) {
                    $('#addNew').modal('hide');
                    alertResponseSuccess('Cập nhật thành công!').then((value) => {
                        if(value) {
                            return this.getAllCategories();
                        }
                    });
                }
            }).catch((response) => {
                $(".app-content .content-body").LoadingOverlay("hide");
                alertResponseError(error);
            });
        },
        editModal(category) {
            this.category = {};
            this.editmode = true;
            $('#addNew').modal('show');
            this.category = category;
        },
        newModal(){
            this.editmode = false;
            this.category = {};
            $('#addNew').modal('show');
        },
    },
    computed: {
        categoryNameErrorMsg() {
            if(!this.$v.category.name.required) {
                return  this.messages.EMAIl_REQUIRED;
            }
            if(this.errors && this.errors.name) {
                return this.errors.name[0];
            }
            return null;
        }
    },
    created() {
        this.getAllCategories();
    },
}
