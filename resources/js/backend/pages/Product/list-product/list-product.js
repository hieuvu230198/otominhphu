import ProductService from "../../../services/product.service";
import { alertResponseError, alertResponseSuccess } from "../../../helpers/alertMessage";
import CategoryService from "../../../services/category.service";
import { integer, required} from "vuelidate/lib/validators";
import { messages } from "../../../common/message";
import SupplierService from "../../../services/supplier.service";
import {checkRoleAdmin} from "../../../helpers/checkRole";
export default {
    mixins: [checkRoleAdmin],
    data() {
        return {
            isAdmin: false,
            isCreate: true,
            arrProducts: [],
            arrCategories: [],
            arrSuppliers: [],
            messages: messages,
            formSubmitted: false,
            pagination : {
                currentPage: 1,
                pageCount: 1,
            },
            product: {
                'pro_brand': '',
                'pro_code': '',
                'pro_category_id': 1,
                'pro_name': '',
                'pro_price': '',
                'pro_quantity': '',
                'pro_wattage' : '',
                'pro_height_weight' : '',
                'pro_supplier_id': 1,
            },
            productValidate: {
                'pro_brand': '',
                'pro_code': '',
                'pro_category_id': 1,
                'pro_name': '',
                'pro_price': '',
                'pro_quantity': '',
                'pro_wattage' : '',
                'pro_height_weight' : '',
                'pro_supplier_id': 1,
            },
            imagePreview: null,
            dataSearch:'',
        }
    },
    methods: {
        searchData() {
            if (this.dataSearch) {
                return ProductService.searchProduct({key: this.dataSearch}).then((response) => {
                    if(response.data.success) {
                        this.pagination.pageCount = response.data.data.last_page;
                        this.pagination.currentPage = response.data.data.current_page;
                        this.arrProducts = response.data.data.data;
                    }
                }).catch((error) => {
                    alertResponseError(error);
                })
            }
        },
        reloadData() {
            this.getAllProducts();
            this.getAllCategories();
            this.getAllSuppliers();
        },
        getAllProducts() {
            ProductService.getAllProducts(this.pagination.currentPage).then((response) => {
                this.pagination.pageCount = response.data.data.last_page;
                this.pagination.currentPage = response.data.data.current_page;
                this.arrProducts = response.data.data.data;
            }).catch((error) => {
                return alertResponseError(error.response.data.message);
            })
        },
        getAllCategories() {
            CategoryService.getAllCategories().then(response => {
                if(response.data.success) {
                    this.arrCategories = response.data.data;
                }
            }).catch((error) => {
                return alertResponseError(error.response.data.message);
            });
        },
        getAllSuppliers() {
            SupplierService.getAllSuppliers().then((response) => {
                if(response.data.success) {
                    this.arrSuppliers = response.data.data;
                }
            }).catch((error) => {
                return alertResponseError(error.response.data.message);
            });
        },
        handleForm() {
            this.formSubmitted = true;
            this.$v.$touch();
            if (this.$v.$invalid) {
                return false;
            } else {
                const productFormData = new FormData(this.$refs.formProduct);
                if (this.isCreate) {
                    this.createProduct(productFormData);
                } else {
                    this.updateProduct(productFormData);
                }
            }
        },
        createProduct(data) {
            ProductService.createProduct(data).then((response) => {
                this.formSubmitted = false;
                $('#addNew').modal('hide');
                if (response.data.success) {
                    alertResponseSuccess(this.messages.CREATE_SUCCESS).then((value) => {
                        if(value) {
                            return this.reloadData();
                        }
                    });
                }
            }).catch((error) => {
                return alertResponseError(error.response.data.message);
            });
        },
        updateProduct(data) {
            ProductService.updateProduct(data, this.product.pro_id).then((response) => {
                $('#addNew').modal('hide');
                this.formSubmitted = false;
                if (response.data.success) {
                    alertResponseSuccess(this.messages.UPDATE_SUCCESS).then((value) => {
                        if(value) {
                            return this.reloadData();
                        }
                    });
                }
            }).catch((error) => {
                return alertResponseError(error.response.data.message);
            })
        },
        deleteProduct(productId) {
            Swal.fire({
                text: 'Xóa sản phẩm này?',
                icon: 'info',
                showCloseButton: true,
                showCancelButton: true,
                showConfirmButton: true,
            }).then((result) => {
                if (result.value) {
                    ProductService.deleteProduct(productId).then((response) => {
                        if (response.data.success) {
                            alertResponseSuccess(this.messages.DELETE_SUCCESS).then((value) => {
                                if(value) {
                                    return this.reloadData();
                                }
                            });
                        }
                    }).catch((error) => {
                        return alertResponseError(error.response.data.message);
                    });
                }
            });
        },
        viewProduct(product) {
            this.product = product;
            $('#viewProduct').modal('show');
        },
        newModal() {
            this.imagePreview = '';
            this.productValidate = {};
            this.product = {};
            this.isCreate = true;
            this.$refs.inputFileImageProduct.value = null;
            $('#addNew').modal('show');
        },
        editModal(product) {
            this.imagePreview = '';
            this.isCreate = false;
            this.productValidate = {};
            this.$refs.inputFileImageProduct.value = null;
            this.product = product;
            this.productValidate = product;
            $('#addNew').modal('show');
        },
        onPreviewImageUpload(e){
            let file = e.target.files[0] || e.dataTransfer.file[0];
            let reader = new FileReader();
            reader.onloadend = () => {
                this.imagePreview = reader.result;
            };
            reader.readAsDataURL(file);
        },
    },
    computed: {
        proCodeErrorMsg() {
            if(!this.$v.productValidate.pro_code.required) {
                return  this.messages.PRODUCT_CODE_REQUIRED;
            }
            if(this.errors && this.errors.pro_code) {
                return this.errors.pro_code[0];
            }
            return null;
        },
        proNameErrorMsg() {
            if(!this.$v.productValidate.pro_name.required) {
                return  this.messages.PRODUCT_NAME_REQUIRED;
            }
            if(this.errors && this.errors.pro_name) {
                return this.errors.pro_name[0];
            }
            return null;
        },
        proCategoryErrorMsg() {
            if(!this.$v.productValidate.pro_category_id.required) {
                return  this.messages.PRODUCT_CATEGORY_REQUIRED;
            }
            if(this.errors && this.errors.pro_category_id) {
                return this.errors.pro_category_id[0];
            }
            return null;
        },
        proBrandErrorMsg() {
            if(!this.$v.productValidate.pro_brand.required) {
                return  this.messages.PRODUCT_BRAND_REQUIRED;
            }
            if(this.errors && this.errors.pro_brand) {
                return this.errors.pro_brand[0];
            }
            return null;
        },
        proDiscountErrorMsg() {
            if(!this.$v.productValidate.pro_discount.integer) {
                return  this.messages.VALUE_NOT_INTEGER;
            }
            if(this.errors && this.errors.pro_discount) {
                return this.errors.pro_discount[0];
            }
            return null;
        },
        proPriceErrorMsg() {
            if(!this.$v.productValidate.pro_price.required) {
                return  this.messages.PRODUCT_BRAND_REQUIRED;
            }
            if(!this.$v.productValidate.pro_price.integer) {
                return  this.messages.VALUE_NOT_INTEGER;
            }
            if(this.errors && this.errors.pro_price) {
                return this.errors.pro_price[0];
            }
            return null;
        },
        proQuantityErrorMsg() {
            if(!this.$v.productValidate.pro_quantity.required) {
                return  this.messages.PRODUCT_QUANTITY_REQUIRED;
            }
            if(!this.$v.productValidate.pro_quantity.integer) {
                return  this.messages.VALUE_NOT_INTEGER;
            }
            if(this.errors && this.errors.pro_quantity) {
                return this.errors.pro_quantity[0];
            }
            return null;
        },
    },
    validations() {
        return {
            productValidate: {
                pro_code: { required },
                pro_name: { required },
                pro_category_id: { required },
                pro_discount: { integer },
                pro_price: { required, integer },
                pro_brand: { required },
                pro_quantity: { required, integer },
                pro_supplier_id: { required, integer},
            }
        }
    },
    created() {
        const loginUser = JSON.parse(localStorage.getItem('user'));
        this.isAdmin = this.checkRoleAdmin(loginUser);
        this.reloadData();
    },
}
