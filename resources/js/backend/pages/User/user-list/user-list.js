import { required, email, minLength, integer, sameAs } from "vuelidate/lib/validators";
import { alertResponseError, alertResponseSuccess } from "../../../helpers/alertMessage";
import {ROLES, USER_ROLE_DEFAULT} from "../../../common/constant";
import { messages } from "../../../common/message";
import UserService from "../../../services/user.service";
export default {
    data() {
        return {
            messages: messages,
            roles: ROLES,
            userFormData: new FormData,
            formSubmitted: false,
            imagePreview: null,
            user: {},
            errors: {},
            pagination : {
                currentPage: 1,
                pageCount: 1,
            },
            users: {},
            dataSearch:'',
        }
    },
    validations() {
        return {
            user: {
                name: { required },
                username: { required },
                email: { required, email },
                phone: { required, integer, minLength: minLength(9) },
                password: { required, minLength: minLength(6),
                    isInvalid(value) {
                        return /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/.test(value);
                    }
                },
                confirm_password: { required, sameAsPassword: sameAs('password')  },
            }
        }
    },
    created() {
        this.reloadData();
    },
    methods: {
        searchData() {
            if (this.dataSearch) {
                return UserService.searchUser({key: this.dataSearch}).then((response) => {
                    if(response.data.success) {
                        this.pagination.pageCount = response.data.data.last_page;
                        this.pagination.currentPage = response.data.data.current_page;
                        this.users = response.data.data.data;
                    }
                }).catch((error) => {
                    alertResponseError(error);
                })
            }
        },
        reloadData() {
            this.getAllUser();
        },
        getAllUser() {
            UserService.getAllUsers(this.pagination.currentPage).then((response) => {
                this.pagination.pageCount = response.data.data.last_page;
                this.pagination.currentPage = response.data.data.current_page;
                this.users = response.data.data.data;
            }).catch((error) => {
                return alertResponseError(error.response.data.message);
            })
        },
        onPreviewImageUpload(e){
            let file = e.target.files[0] || e.dataTransfer.file[0];
            let reader = new FileReader();
            reader.onloadend = () => {
                this.imagePreview = reader.result;
            };
            reader.readAsDataURL(file);
        },
        handleForm() {
            this.formSubmitted = true;
            this.$v.$touch();
            if (this.$v.$invalid) {
                return false;
            } else {
                this.userFormData = new FormData(this.$refs.formUser);
                this.createUser();
            }
        },
        createUser(){
            UserService.createUser(this.userFormData).then((response) => {
                this.formSubmitted = false;
                if (response.data.success) {
                    alertResponseSuccess(this.messages.USER_CREATE_SUCCESS);
                    this.reloadData();
                    this.resetDataModal();
                }
            }).catch((error) => {
                // case don't have response from server
                if(!error.response || !error.response.data){
                    alertResponseError(error);
                    return;
                }
                if(error.response.data.errors) {
                    const rawErrors = error.response.data.errors;
                    this.errors = rawErrors ? rawErrors : {};
                    alertResponseError(error.response.data.message);
                    return;
                }
                this.errors = error;
                alertResponseError(error);
            })
        },
        deleteUser(userId) {
            Swal.fire({
                text: 'Xóa tài khoản này?',
                icon: 'info',
                showCloseButton: true,
                showCancelButton: true,
                showConfirmButton: true,
            }).then((result) => {
                if (result.value) {
                    UserService.deleteUser(userId).then((response) => {
                        if (response.data.success) {
                            alertResponseSuccess(this.messages.USER_DELETE_SUCCESS).then((value) => {
                                if(value) {
                                    return this.reloadData();
                                }
                            });
                        }
                    }).catch((error) => {
                        return alertResponseError(error.response.data.message);
                    });
                }
            });
        },
        newModal() {
            this.resetDataModal();
            $('#addNew').modal('show');
        },
        resetDataModal() {
            $('#addNew').modal('hide');
            this.userFormData = new FormData;
            this.user = {
                role_id: USER_ROLE_DEFAULT
            };
            this.$refs.inputFileAvatar.value = null;
            this.imagePreview = null;
        }
    },
    computed: {
        emailErrorMsg() {
            if(!this.$v.user.email.required) {
                return  this.messages.EMAIl_REQUIRED;
            }
            if(!this.$v.user.email.email) {
                return this.messages.EMAIL_INVALID;
            }
            if(this.errors && this.errors.email) {
                return this.errors.email[0];
            }
            return null;
        },
        passwordError(){
            if(!this.$v.user.password.required){
                return this.messages.PASSWORD_REQUIRED;
            }
            if(!this.$v.user.password.isInvalid){
                return this.messages.PASSWORD_INVALID;
            }
            if(this.errors && this.errors.password){
                return this.errors.password;
            }
            return null;
        },
    },
}
