import {required, email, minLength, integer, sameAs} from "vuelidate/lib/validators";
import { alertResponseError, alertResponseSuccess } from "../../../helpers/alertMessage";
import { messages } from "../../../common/message";
import { ROLES } from "../../../common/constant";
import UserService from "../../../services/user.service";
import {AuthService} from "../../../services/auth.service";
export default {
    data() {
        return {
            roles: ROLES,
            isChangePassword: false,
            messages: messages,
            userFormData: new FormData,
            errorsResponse: [],
            formSubmitted: false,
            userId: null,
            user: {
                name: '',
                username: '',
                email: '',
                phone: '',
                password: '',
                password_old: '',
                password_new: '',
                confirm_password: ''
            },
            errors: {},
        }
    },
    methods: {
        reloadData() {
            $(".app-content .content-body").LoadingOverlay("show");
            this.getUser();
        },
        getUser() {
            UserService.getUserWithId(this.userId).then((response) => {
                $(".app-content .content-body").LoadingOverlay("hide");
                if(response.data.success) {
                    this.user = response.data.data;
                }
            }).catch((error) => {
                alertResponseError(error);
            });
        },
        handleForm() {
            this.formSubmitted = true;
            this.$v.$touch();
            if (this.$v.$invalid) {
                return false;
            } else {
                this.userFormData = new FormData(this.$refs.formDetailUser);
                this.updateUser();
            }
        },
        updateUser(){
            UserService.updateUser(this.userId, this.userFormData).then((response) => {
                this.formSubmitted = false;
                if (response.data.success) {
                    const userId = JSON.parse(AuthService.getUserCurrentLogin()).id;
                    AuthService.resetUserLocalstorage(userId);
                    alertResponseSuccess(this.messages.USER_UPDATE_SUCCESS);
                    this.getUser();
                }
            }).catch((error)=> {
                if(!error.response || !error.response.data){
                    alertResponseError(error);
                    return;
                }
                if(error.response.data.errors) {
                    const rawErrors = error.response.data.errors;
                    this.errors = rawErrors ? rawErrors : {};
                    alertResponseError(error.response.data.message);
                    return;
                }
                this.errors = error;
                alertResponseError(error);
            })
        },
        openModalChangePassword() {
            $('#modal-change-password').modal('show');
            this.isChangePassword = false;
        },
        updatePassword(userId) {
            UserService.changePasswordUser({password_old: this.user.password_old, password_new: this.user.password_new}, userId).then((response) => {
                $('#modal-change-password').modal('hide');
                if (response.data.success) {
                    AuthService.resetUserLocalstorage(userId);
                    alertResponseSuccess(this.messages.CHANGE_PASSOWORD_SUCCESS);
                    this.getUser();
                }
            }).catch((error) => {
                if(!error.response || !error.response.data){
                    alertResponseError(error);
                    return;
                }
                alertResponseError(error.response.data.message);
            })
        },
        deleteUser(userId) {
            Swal.fire({
                text: 'Bạn có chắc xóa người dùng này?',
                icon: 'info',
                showCloseButton: true,
                showCancelButton: true,
                showConfirmButton: true,
            }).then((result) => {
                if (result.value) {
                    UserService.deleteUser(userId).then((response) => {
                        if (response.data.success) {
                            alertResponseSuccess(this.message.USER_DELETE_SUCCESS).then((value) => {
                                if(value) {
                                    return this.reloadData();
                                }
                            });
                        }
                    }).catch((error) => {
                        return alertResponseError(error.response.data.message);
                    });
                }
            });
        },
        goBack() {
            this.$router.push({name: 'admin-user-list'});
        }
    },
    computed: {
        emailErrorMsg() {
            if(!this.$v.user.email.required) {
                return  this.messages.EMAIl_REQUIRED;
            }
            if(!this.$v.user.email.email) {
                return this.messages.EMAIL_INVALID;
            }
            if(this.errors && this.errors.email) {
                return this.errors.email[0];
            }
            return null;
        },
        usernameErrorMsg(){
            if (!this.$v.user.username.required){
                return this.messages.USERNAME_REQUIRED;
            }
            if(this.errors && this.errors.username) {
                return this.errors.username[0];
            }
            return null;
        },
        phoneErrorMsg(){
            if (!this.$v.user.phone.required){
                return this.messages.PHONE_REQUIRED;
            }
            if (!this.$v.user.phone.integer || !this.$v.user.phone.minLength){
                return this.messages.PHONE_INVALID;
            }
            if (this.errors && this.errors.phone) {
                return this.errors.phone[0];
            }
            return null;
        },
        departmentErrorMsg() {
            if (!this.$v.user.department.required){
                return this.messages.PHONE_REQUIRED;
            }
            if (this.errors && this.errors.department) {
                return this.errors.department[0];
            }
            return null;
        },
        passwordOldError() {
            if (!this.$v.user.password_old.required) {
                return this.messages.PASSWORD_OLD_REQUIRED;
            }
            if (this.errors && this.errors.password_old) {
                return this.errors.password_old[0];
            }
            return null;
        },
        passwordNewError() {
            if (!this.$v.user.password_new.required) {
                return this.messages.PASSWORD_NEW_REQUIRED
            }
            if (!this.$v.user.password_new.isInvalid) {
                return this.messages.PASSWORD_INVALID;
            }
            return null;
        },
        passwordConfirmError() {
            if (!this.$v.user.confirm_password.required) {
                return this.messages.CONFIRM_PASSWORD_REQUIRED;
            }
            if (!this.$v.user.confirm_password.sameAsPassword) {
                return this.messages.CONFIRM_PASSWORD_NOT_MATCH;
            }
            return null;
        },
    },
    validations() {
        if (this.isChangePassword) {
            return {
                user: {
                    password_old: { required },
                    password_new: {
                        required,
                        isInvalid(value) {
                            return /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/.test(value);
                        }
                    },
                    confirm_password: { required, sameAsPassword: sameAs('password') },
                }
            }
        } else {
            return {
                user: {
                    name: { required },
                    username: { required },
                    department: { required },
                    email: { required, email },
                    phone: { required, integer, minLength: minLength(9) },
                }
            }
        }
    },
    created() {
        this.userId = this.$route.params.userId;
        this.reloadData();
    },
}
