export const ROLES = [
    { name: 'admin', value: 1 },
    { name: 'user', value: 2 },
];

export const USER_ROLE_DEFAULT = 2;

export const BASE_URL = process.env.MIX_APP_URL;
export const API_URL = process.env.MIX_API_URL;
export const API_AUTH_URL = process.env.MIX_API_AUTH_URL;
