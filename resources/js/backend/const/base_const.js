export const COMMON = {
    USER_ROLE_ADMIN: 'admin',
    USER_ROLE_NORMAL: 'user',
};

export const ROLES = {
    USER_ROLE_ADMIN: 'admin',
    USER_ROLE_NORMAL: 'user',
};
