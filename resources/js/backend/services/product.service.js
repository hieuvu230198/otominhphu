import ApiService from "./api.service";

const ProductService = {
    getAllProducts(page) {
        return ApiService.getWithAuth('/product/get-all-products?page=' + page);
    },

    createProduct(data) {
        return ApiService.post('/product/create', data);
    },

    updateProduct(data, productId) {
        return ApiService.post('/product/update/' + productId, data);
    },

    deleteProduct(userId) {
        return ApiService.delete('/product/delete/' + userId);
    },

    searchProduct(key) {
        return ApiService.post('/product/search', key);
    }
};

export default ProductService
