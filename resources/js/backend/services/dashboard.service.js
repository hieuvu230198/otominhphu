import ApiService from "./api.service";
import { AuthService } from "./auth.service";
const DashBoardService = {
    getDataDashBoard() {
        return ApiService.getWithAuth("/home/get-data-dashboard");
    }
};
export default DashBoardService;
