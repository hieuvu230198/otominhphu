import {AuthService} from "./auth.service";
import {API_URL} from "../common/constant";


const ApiService = {
    get(resource) {
        return axios.get(resource)
    },

    getWithAuth(resource) {
        const accessToken = AuthService.getAccessToken();
        return axios.get(API_URL + resource, {headers: {'Authorization': `Bearer ${accessToken}`}})
    },

    post(resource, data) {
        const accessToken = AuthService.getAccessToken();
        return axios.post(API_URL + resource, data, {headers: {'Authorization': `Bearer ${accessToken}`}})
    },

    put(resource, data) {
        const accessToken = AuthService.getAccessToken();
        return axios.put(API_URL + resource, data, {headers: {'Authorization': `Bearer ${accessToken}`}})
    },

    delete(resource) {
        const accessToken = AuthService.getAccessToken();
        return axios.delete(API_URL + resource, {headers: {'Authorization': `Bearer ${accessToken}`}})
    },
};
export default ApiService
