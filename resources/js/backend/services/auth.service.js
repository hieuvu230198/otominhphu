import UserService from "./user.service";
import { alertResponseError } from "../helpers/alertMessage";
import {API_AUTH_URL} from "../common/constant";
const TOKEN_KEY = 'access_token';
const USER_CURRENT_LOGIN = 'user';

const AuthService = {
    login(credentials) {
      return axios.post(API_AUTH_URL + '/login', credentials)
    },
    getAccessToken() {
        return localStorage.getItem(TOKEN_KEY);
    },
    saveToken(token) {
        return localStorage.setItem(TOKEN_KEY, token);
    },
    getUserCurrentLogin() {
        return localStorage.getItem(USER_CURRENT_LOGIN);
    },
    saveUserCurrentLogin(user) {
        return localStorage.setItem(USER_CURRENT_LOGIN, JSON.stringify(user));
    },
    removeToken() {
        localStorage.removeItem(TOKEN_KEY);
        localStorage.removeItem(USER_CURRENT_LOGIN);
        return true;
    },
    logout() {
        localStorage.removeItem(TOKEN_KEY);
        localStorage.removeItem(USER_CURRENT_LOGIN);
        this.$router.go({name: 'admin-login'});
        return true;
    },
    resetUserLocalstorage(userId) {
        return UserService.getAuthMe(userId).then((response) => {
            this.saveUserCurrentLogin(response.data.data);
        }).catch((error) => {
            if (error.response.status === 401) {
                alertResponseError(error.response.data.message).then((value) => {
                    if(value) {
                        return this.logout();
                    }
                });
            } else {
                alertResponseError(error);
            }
        });
    }
};

export { AuthService }
