import ApiService from "./api.service";

const CategoryService = {
    getAllCategories() {
        return ApiService.getWithAuth('/category/get-all-categories');
    },

    createCategory(data) {
        return ApiService.post('/category/create', data);
    },

    updateCategory(categoryId, data) {
        return ApiService.post('/category/update/' + categoryId, data);
    },

    deleteCategory(categoryId) {
        return ApiService.delete('/category/delete/' + categoryId);
    },

    searchCategory(key) {
        return ApiService.post('/category/search', key);
    }
};

export default CategoryService
