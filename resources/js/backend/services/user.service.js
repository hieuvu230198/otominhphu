import ApiService from "./api.service";

const UserService = {
    getAllUsers(page) {
        return ApiService.getWithAuth('/user/get-all-users?page=' + page);
    },

    getUserWithId(userId) {
        return ApiService.getWithAuth('/user/get-user-with-id/' + userId);
    },

    getAuthMe(userId) {
        return ApiService.post('/auth/me', {id: userId});
    },

    createUser(data) {
        return ApiService.post('/user/create', data);
    },

    updateUser(userId, data) {
        return ApiService.post('/user/update/' + userId, data);
    },

    changePasswordUser(data, userId) {
        return ApiService.put('/user/' + userId + '/change-password', data);
    },

    deleteUser(userId) {
        return ApiService.delete('/user/delete/' + userId);
    },

    searchUser(key) {
        return ApiService.post('/user/search', key);
    }
};

export default UserService
