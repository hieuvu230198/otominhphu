import ApiService from "./api.service";

const SupplierService = {
    getAllSuppliers() {
        return ApiService.getWithAuth('/supplier/get-all-suppliers');
    },
    createSupplier(data) {
        return ApiService.post('/supplier/create', data);
    },

    updateSupplier(supplierId, data) {
        return ApiService.post('/supplier/update/' + supplierId, data);
    },

    deleteSupplier(supplierId) {
        return ApiService.delete('/supplier/delete/' + supplierId);
    },
    searchSupplier(key) {
        return ApiService.post('/supplier/search', key);
    }
};

export default SupplierService
