import ApiService from "./api.service";

const BillService = {
    getAllBills(page) {
        return ApiService.getWithAuth('/bill/get-all-bills?page=' + page);
    },

    getBillWithId(billId) {
        return ApiService.getWithAuth('/bill/get-bill-with-id/' + billId);
    },

    getAllProducts() {
        return ApiService.getWithAuth('/bill/get-all-products');
    },

    updateStatusBill(data) {
        return ApiService.post('/bill/change-status-id', data);
    },

    deleteBill(billId) {
        return ApiService.delete('/bill/delete/' + billId);
    },

    searchBills(key) {
        return ApiService.post('/bill/search', key);
    },

    sendMail(data) {
        return ApiService.post('/bill/send-mail', data);
    }
};

export default BillService
