import ApiService from "./api.service";

const SliderService = {
    getAllSliders(page) {
        return ApiService.getWithAuth('/slider/get-all-sliders?page=' + page);
    },

    createSlider(data) {
        return ApiService.post('/slider/create', data);
    },

    updateSlider(sliderId, data) {
        return ApiService.post('/slider/update/' + sliderId, data);
    },

    deleteSlider(sliderId) {
        return ApiService.delete('/slider/delete/' + sliderId);
    },

    searchSlider(key) {
        return ApiService.post('/slider/search', key);
    }
};

export default SliderService
