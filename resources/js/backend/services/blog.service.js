import ApiService from "./api.service";
import {AuthService} from "./auth.service";

const BlogService = {
    getAllBlogs(page) {
        return ApiService.getWithAuth('/blog/get-all-blogs?page=' + page);
    },

    createBlog(data) {
        data.append('blog_user_id', JSON.parse(AuthService.getUserCurrentLogin()).id);
        return ApiService.post('/blog/create', data);
    },

    updateBlog(blogId, data) {
        return ApiService.post('/blog/update/' + blogId, data);
    },

    deleteBlog(blogId) {
        return ApiService.delete('/blog/delete/' + blogId);
    },

    searchSlider(key) {
        return ApiService.post('/blog/search', key);
    }
};

export default BlogService
