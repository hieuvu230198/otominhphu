/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
import { Form, HasError, AlertError } from 'vform';
window.Form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
Vue.component('user-component', require('./pages/User/user-list/UserList.vue').default);
Vue.component('user-detail-component', require('./pages/User/user-detail/UserDetail.vue').default);
Vue.component('category-component', require('./pages/Category/CategoryComponent.vue').default);
Vue.component('bill-component', require('./pages/Bill/BillComponent.vue').default);
Vue.component('bills-component', require('./pages/Bill/bill-detail/BillDetail.vue').default);
Vue.component('product-component', require('./pages/Product/ProductComponent.vue').default);
Vue.component('supplier-component', require('./pages/Supplier/SupplierComponent.vue').default);
Vue.component('blog-component', require('./pages/Blog/BlogComponent.vue').default);
Vue.component('slider-component', require('./pages/Slider/SliderComponent.vue').default);
Vue.component('admin-layout-component', require('./layouts/AdminLayoutComponent.vue').default);
Vue.component('pagination-component', require('./pages/Pagination/PaginationComponent.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vue from 'vue';
// MomentJS
import moment from 'moment';
// Vuelidate
import Vuelidate from 'vuelidate';
Vue.use(Vuelidate);
import CKEditor from 'ckeditor4-vue';
Vue.use(CKEditor);
// Filter
Vue.filter('upText', function(text){
    if(text) {
        return text.charAt(0).toUpperCase() + text.slice(1)
    }
});
Vue.filter('formatPrice', function(value, splitStr = ','){
    if (value === null || value === undefined) {
        return '';
    }
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, splitStr);
});
Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('MM-DD-YYYY')
    }
});
// SweetAlert2
import Swal from 'sweetalert2'
const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timerProgressBar: true,
    timer: 2000,
});
window.toast = toast;
window.Swal = require('sweetalert2');
window.LoadingOverlay = require('gasparesganga-jquery-loading-overlay/dist/loadingoverlay');



// Router
import router from "../backend/router/index";
import {initializeRouter} from "./helpers/initializeRouter";

initializeRouter(router);
const app = new Vue({
    el: '#app',
    router: router,
});


