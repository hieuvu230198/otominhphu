import AdminLayoutComponent from "../layouts/AdminLayoutComponent";
import UserComponent from "../pages/User/user-list/UserList";
import CategoryComponent from "../pages/Category/CategoryComponent";
import DashboardComponent from "../pages/Dashboard/DashboardComponent";
import LoginComponent from "../pages/Auth/Login/LoginComponent";
import ProductComponent from "../pages/Product/ProductComponent";
import SliderComponent from "../pages/Slider/SliderComponent";
import BlogComponent from "../pages/Blog/BlogComponent";
import BillComponent from "../pages/Bill/BillComponent";
import BillDetailComponent from "../pages/Bill/bill-detail/BillDetail";
import SupplierComponent from "../pages/Supplier/SupplierComponent";
import UserDetailComponent from "../pages/User/user-detail/UserDetail";
import User from "../pages/User/User";
import Error404 from "../components/Error404";
import Error401 from "../components/Error401";
import Error500 from "../components/Error500";
import ListProduct from "../pages/Product/list-product/ListProduct";
import ListBlog from "../pages/Blog/list-blog/ListBlog";
import ListSlider from "../pages/Slider/list-slider/ListSlider";
import ProfileComponent from "../pages/Profile/ProfileComponent";
import ListBill from "../pages/Bill/list-bill/ListBill";
const routes = [
    { path: '/admin',         component: LoginComponent },
    { path: "*",        redirect: '/admin/manage/dashboard' },
    { path: "/404-not-found",               name: '404',    component: Error404 },
    { path: "/401-unauthorization",         name: '401',    component: Error401 },
    { path: "/500-internal-server-error",   name: '500',    component: Error500 },
    { path: '/admin',   component: LoginComponent,
        children:
        [
            { path: 'login',    name: 'admin-login',    component: LoginComponent },
            { path: 'logout',   name: 'admin-logout',   component: LoginComponent  }
        ]
    },
    { path: '/admin/manage', component: AdminLayoutComponent, meta: { requiresAuth: true },
        children:
        [
            { path: 'dashboard', name: 'admin-dashboard', component: DashboardComponent },
            { path: 'profile', name: 'admin-profile', component: ProfileComponent },
            {
                path: 'users',   name: 'admin-user', component: User, redirect: 'users/list-user', meta: {admin: true},
                children: [
                    { path: 'list-user', name: 'admin-user-list', component: UserComponent },
                    { path: 'detail/:userId', name: 'admin-user-detail', component: UserDetailComponent },
                ]
            },
            {
                path: 'slider', name: 'admin-slider', component: SliderComponent, redirect: 'slider/list-slider',
                children: [
                    { path: 'list-slider', name: 'admin-slider-list', component: ListSlider },
                ]
            },
            {
                path: 'blog', name: 'admin-blog', component: BlogComponent, redirect: 'blog/list-blog',
                children: [
                    { path: 'list-blog', name: 'admin-blog-list', component: ListBlog },
                    { path: 'detail/:blogId', component: UserDetailComponent },
                ]
            },
            { path: 'supplier', name: 'admin-supplier', component: SupplierComponent },
            {
                path: 'bills', name: 'admin-bill', component: BillComponent, redirect: 'bills/list-bill',
                children: [
                    { path: 'list-bill', name: 'admin-bill-list', component:  ListBill},
                    { path: 'detail/:billId', name: 'admin-bill-detail', component: BillDetailComponent },
                ]
            },
            { path: 'categories', name: 'admin-category', component: CategoryComponent },
            {
                path: 'products', name: 'admin-product', component: ProductComponent, redirect: 'products/list-product',
                children: [
                    { path: 'list-product', name: 'admin-product-list', component: ListProduct },
                ]
            },
        ]
    },
];
export default routes
