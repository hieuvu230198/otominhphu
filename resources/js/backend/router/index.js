import Vue from "vue";
import VueRouter from "vue-router";
import routes from "../../backend/router/routes";
Vue.use(VueRouter);
// configure router
const router =  new VueRouter({
    mode: 'history',
    routes,
    linkActiveClass: 'active',
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    },
});
export default router
