export function alertResponseError(error) {
    let msgError = "";
    if(error.response && error.response.data){
        msgError = error.response.data.msg;
    } else  {
        msgError = error;
    }
    return Swal.fire({
        text: `${msgError}`,
        icon: 'error',
    });
}

export function alertResponseSuccess(message) {
    return Swal.fire({
        text: `${message}`,
        icon: 'success',
    });
}
