import {AuthService} from "../services/auth.service";
import axios from "axios";
import {alertResponseError} from "./alertMessage";
export function initializeRouter(router) {
    router.beforeEach((to, from, next) => {
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        const isAdmin = to.matched.some(record => record.meta.admin);
        const loggedIn = AuthService.getAccessToken();
        const user = JSON.parse(AuthService.getUserCurrentLogin());
        // login failed
        if (requiresAuth && !loggedIn) {
            return next({name: 'admin-login'});
        }
        // check url current  = /admin/login
        if (to.path === '/admin/login' && loggedIn && user) {
            return next({ name: 'admin-dashboard' });
        }
        if (to.path === '/admin' && loggedIn && user) {
            return next({ name: 'admin-dashboard' });
        }
        // Enter url : /admin/logout
        if (to.path === '/admin/logout') {
            AuthService.removeToken();
            return next({name: 'admin-login'});
        }
        if (requiresAuth && loggedIn && user && isAdmin) {
            if (user.roleName === 'admin') {
                return next();
            }
            else {
                return next({name: '401'});
            }
        }
        return next();
    });
}
