import {ROLES} from "../const/base_const";

export const checkRoleAdmin = {
    methods: {
        checkRoleAdmin(loginUser) {
            return loginUser && loginUser.roleName === ROLES.USER_ROLE_ADMIN;
        },
    }
};
