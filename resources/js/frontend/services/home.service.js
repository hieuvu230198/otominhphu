import ApiService from "./api.service";

const HomeService = {
    getProductNews(offset) {
        return ApiService.get('/get-all-products/' + offset);
    },

    getProductOlds(offset) {
        return ApiService.get('/get-old-products/' + offset);
    },

    getSliderNews(offset) {
        return ApiService.get('/get-all-sliders/' + offset);
    }
};

export default HomeService
