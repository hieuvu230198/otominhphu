import ApiService from "./api.service";

const ProductService = {
    getRelatedProducts(categoryId) {
        return ApiService.get('/get-related-product/' + categoryId);
    },
    getProductByCategory(categoryId) {
        return ApiService.get('/get-product-by-category/' + categoryId);
    },
    getProductNews(offset) {
        return ApiService.get('/get-all-products/' + offset);
    },
    getProductNewsDetail() {
        return ApiService.get('/get-new-detail-products/');
    },
};

export default ProductService
