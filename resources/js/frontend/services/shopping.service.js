import ApiService from "./api.service";

const ShoppingService = {
    getProductNews(page) {
        return ApiService.get('/get-new-products?page=' + page);
    },

    searchProduct(data) {
        if (data.key) {
            return ApiService.get('/search?category=' + data.category+ '&key=' + data.key);
        } else {
            return ApiService.get('/search?category=' + data.category);
        }
    }
};

export default ShoppingService
