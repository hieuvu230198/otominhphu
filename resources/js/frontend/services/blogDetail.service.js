import ApiService from "./api.service";

const BlogDetailService = {
    getBlogDetail(id) {
        return ApiService.get('/get-detail-blog/' + id);
    },
};
export default BlogDetailService
