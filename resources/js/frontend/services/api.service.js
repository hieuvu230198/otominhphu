import {API_URL} from "../common/const";

const ApiService = {
    get(resource) {
        return axios.get(API_URL + resource);
    },
    post(resource, data) {
        return axios.post(API_URL + resource, data);
    }
};
export default ApiService
