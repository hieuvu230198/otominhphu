import ApiService from "./api.service";

export const CartService = {
    getCartLocalStorage() {
        return localStorage.getItem('carts');
    },
    storeCartLocalStorage(carts) {
        return localStorage.setItem('carts', carts);
    },
    postCart(cart) {
        return ApiService.post('/post-cart', cart);
    },
    removeCart() {
        return localStorage.removeItem('carts');
    }
};

export default CartService
