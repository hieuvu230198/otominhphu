import ApiService from "./api.service";

const BlogService = {
    getAllBlogs(offset) {
        return ApiService.get('/get-all-blogs/' + offset);
    },
};
export default BlogService

