import ApiService from "./api.service";

const CategoryService = {
    getAllCategories(page) {
        return ApiService.get('/get-all-categories');
    },
};

export default CategoryService
