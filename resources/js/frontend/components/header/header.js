import CategoryService from "../../services/category.service";
import {BASE_URL} from "../../common/const";

export default {
    data() {
        return {
            categories: {},
            categoryId: '',
            carts: [],
            totalPrice: 0,
            keySearch: '',
            keyCategory: ''
        }
    },
    methods: {
        setCategoryId(e) {
            this.categoryId = e.target.value;
        },
        searchData() {
            let key = this.keySearch;
            let category = this.keyCategory;
            if(category === ''){
                location.href = BASE_URL + '/search?key='+ key + '&category=all';
            }else{
                location.href = BASE_URL + '/search?key='+ key + '&category=' + category;
            }
        },
        getAllCategories() {
            CategoryService.getAllCategories().then((response) => {
                this.categories = response.data.data;
            }).catch((error) => {
                console.log(error);
            })
        },
        reloadCartData() {
            this.carts = JSON.parse(localStorage.getItem('carts'));
        },
        removeItemCart(item) {
            this.$emit('removeitemcart', item);
            this.reloadCartData();
        }
    },
    created() {
        this.getAllCategories();
        this.carts = JSON.parse(localStorage.getItem('carts')) ? JSON.parse(localStorage.getItem('carts')) : [];
    },
    computed: {
        getAllCart() {
            return this.carts;
        },
        total() {
            let total = 0;
            if (this.carts) {
                total = this.carts.reduce((total, item) => {
                    return total + item.quantity * item.price;
                }, 0)
            }
            return this.totalPrice = total;
        }
    }
}
