import Vue from "vue";

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
Vue.component('app-component', require('./layouts/App').default);
Vue.component('header-component', require('./components/header/HeaderComponent').default);
Vue.component('footer-component', require('./components/footer/FooterComponent').default);
Vue.component('home-component', require('./pages/home/HomeComponent').default);
Vue.component('blog-component', require('./pages/blog/list-blog/BlogComponent').default);
Vue.component('shopping-component', require('./pages/shopping/list-shopping/ShoppingComponent').default);
Vue.component('blogdetail-component', require('./pages/blog/list-blog/DetailComponent').default);
Vue.component('product-detail-component', require('./pages/product/ProductDetailComponent.vue').default);
Vue.component('product-by-category-component', require('./pages/category/ProductByCategoryComponent.vue').default);
Vue.component('contact-component', require('./pages/contact/ContactComponent.vue').default);
Vue.component('about-us-component', require('./pages/aboutus/AboutUSComponent.vue').default);
Vue.component('cart-component', require('./pages/cart/ViewCartComponent').default);
Vue.component('search-component', require('./pages/search/SearchComponent').default);
Vue.component('pagination-component', require('./pages/Pagination/PaginationComponent.vue').default);
// MomentJS
import moment from 'moment';
// Vuelidate
import Vuelidate from 'vuelidate';
Vue.use(Vuelidate);
// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
Vue.filter('upText', function(text){
    if(text) {
        return text.charAt(0).toUpperCase() + text.slice(1)
    }
});
Vue.filter('formatPrice', function(value, splitStr = ','){
    if (value === null || value === undefined) {
        return '';
    }
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, splitStr);
});
// SweetAlert2
import Swal from 'sweetalert2'
import {CartService} from "./services/cart.service";
const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timerProgressBar: true,
    timer: 2000,
});
window.toast = toast;
window.Swal = require('sweetalert2');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const app = new Vue({
    el: '#main',
    methods: {
        addToCart() {
            this.$refs.headerComponent.reloadCartData();
        },
        removeItemCart(item) {
            const carts = JSON.parse(CartService.getCartLocalStorage());
            const indexOfItem = carts.findIndex((compareItem)=>{
                return compareItem.id===item.id;
            });
            if (indexOfItem >= 0) {
                carts.splice(indexOfItem, 1);
            }
            CartService.storeCartLocalStorage(JSON.stringify(carts));
            this.$refs.headerComponent.reloadCartData();
        },
        updateQuantity(data) {
            const product = data.product;
            const quantityNew = data.quantity;
            let carts = JSON.parse(CartService.getCartLocalStorage());
            const matchingProductIndex = carts.findIndex((item) => {
                return item.id === product.id;
            });
            if(quantityNew > 0) {
                carts[matchingProductIndex].quantity = quantityNew;
            } else {
                carts[matchingProductIndex].quantity = 1;
            }
            localStorage.setItem('carts', JSON.stringify(carts));
            this.$refs.headerComponent.reloadCartData();
            this.$refs.cartComponent.reloadCartData();
        }
    }
});


