import CategoryService from "../../services/category.service";
import ProductService from "../../services/product.service";
import {BASE_URL} from "../../common/const";

export default {
    data() {
        return {
            productsByCategory: [],
            categories: {},
            carts: [],
            pagination : {
                currentPage: 1,
                pageCount: 1,
            },
            keySearch: '',
            keyCategory: '',
            categoryName: '',
            minPrice: 0,
            maxPrice: 50000000,
        }
    },
    methods: {
        discountedPrice(product) {
            return product.pro_price - (product.pro_price *(product.pro_discount)/100)
        },
        getProductByCategory() {
            let categoryId = window.location.href.split('/').pop();
            return ProductService.getProductByCategory(categoryId).then((response) => {


                if (response.data.success) {
                    this.categorie = categoryId;
                    this.productsByCategory = response.data.data.data;
                    this.pagination.pageCount = response.data.data.last_page;
                    this.pagination.currentPage = response.data.data.current_page;
                }
            }).catch((error) => {
                console.log(error);
            })
        },
        addToCart(product) {
            // check item instance
            let newItemToCartIndex = this.carts.findIndex((item) => {
                return item.id === product.pro_id;
            });
            if (newItemToCartIndex > -1 ) {
                this.carts[newItemToCartIndex].quantity++;
            } else {
                product.quantity = 1;
                if(product.pro_discount == 0){
                    this.carts.push({
                        id: product.pro_id,
                        code: product.pro_code,
                        name: product.pro_name,
                        price: product.pro_price,
                        thumbnail_path: product.thumbnail_path,
                        quantity: product.quantity
                    });
                }else{
                    this.carts.push({
                        id: product.pro_id,
                        code: product.pro_code,
                        name: product.pro_name,
                        price: this.discountedPrice(product),
                        thumbnail_path: product.thumbnail_path,
                        quantity: product.quantity
                    });
                }
            }
            toast.fire({
                text: 'Thêm Sản phẩm thành công!',
                icon: 'success',
            });
            this.storeCart();
            this.$emit('addtocart');
        },
        storeCart() {
            localStorage.setItem('carts', JSON.stringify(this.carts));
        },
        getAllCategories() {
            return CategoryService.getAllCategories().then((response) => {
                this.categories = response.data.data;

                let categoryId = window.location.href.split('/').pop();
                this.categoryName = this.categories[categoryId - 1].name;
            }).catch((error) => {
                console.log(error);
            })
        },
        searchData() {
            let key = this.keySearch;
            let category = this.keyCategory;
            let minPrice = this.minPrice;
            let maxPrice = this.maxPrice;
            if(category === '') {
                location.href = BASE_URL + '/search?key='+ key  + '&minPrice=' + minPrice + '&maxPrice=' + maxPrice + '&category=all';
            } else{
                location.href = BASE_URL + '/search?key='+ key  + '&minPrice=' + minPrice+ '&maxPrice=' + maxPrice + '&category=' + category;
            }
        }
    },
    created() {
        this.getAllCategories();
        this.getProductByCategory();
        this.carts = localStorage.getItem('carts') ? JSON.parse(localStorage.getItem('carts')) : [];
    }
}

