import CategoryService from "../../services/category.service";
import ProductService from "../../services/product.service";

export  default  {
    props: ['productDetail'],
    data() {
        return {
            productNews: [],
            categories: {},
            product: this.productDetail,
            relatedProduct: {},
            carts: [],
        }
    },
    methods: {
        discountedPrice(product) {
            return product.pro_price - (product.pro_price *(product.pro_discount)/100)
        },
        getProductNews() {
            return ProductService.getProductNewsDetail().then((response) => {
                if (response.data.success) {
                    this.productNews = response.data.data;
                }
            }).catch((error) => {
                console.log(error);
            })
        },
        addToCart(product) {
            // check item instance
            let newItemToCartIndex = this.carts.findIndex((item) => {
                return item.id === product.pro_id;
            });
            if (newItemToCartIndex > -1 ) {
                this.carts[newItemToCartIndex].quantity++;
            } else {
                product.quantity = 1;
                if(product.pro_discount == 0){
                    this.carts.push({
                        id: product.pro_id,
                        code: product.pro_code,
                        name: product.pro_name,
                        price: product.pro_price,
                        thumbnail_path: product.thumbnail_path,
                        quantity: product.quantity
                    });
                }else{
                    this.carts.push({
                        id: product.pro_id,
                        code: product.pro_code,
                        name: product.pro_name,
                        price: this.discountedPrice(product),
                        thumbnail_path: product.thumbnail_path,
                        quantity: product.quantity
                    });
                }
            }
            toast.fire({
                text: 'Thêm Sản phẩm thành công!',
                icon: 'success',
            });
            this.storeCart();
            this.$emit('addtocart');
        },
        storeCart() {
            localStorage.setItem('carts', JSON.stringify(this.carts));
        },
        getAllCategories() {
            return CategoryService.getAllCategories().then((response) => {
                this.categories = response.data.data;
            }).catch((error) => {
                console.log(error);
            })
        },
        getRelatedProduct() {
            return ProductService.getRelatedProducts(this.product.category_id).then((response) => {
                if (response.data.success) {
                    this.relatedProduct = response.data.data;
                }
            }).catch((error) => {
                console.log(error);
            })
        }
    },
    created() {
        this.getProductNews();
        this.getAllCategories();
        this.getRelatedProduct();
        this.carts = localStorage.getItem('carts') ? JSON.parse(localStorage.getItem('carts')) : [];
    }
}
