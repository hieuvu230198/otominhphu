import blogDetail from "../../../services/blogDetail.service";
import BlogService from "../../../services/blog.service";

export default {
    data() {
        return {
            blogDetail: [],
            blogNews: [],
        }
    },
    props : ['id'],
    methods: {
        getBlogDetail(id) {
            //let offset = this.blogDetail.length > 0 ? this.blogDetail.length : 0;
            blogDetail.getBlogDetail(id).then((response) => {
                if (response.data.success) {
                    this.blogDetail = response.data.data;
                }
                console.log(this.blogDetail);
            }).catch((error) => {
                console.log(error);
            })
        },
        getAllBlogs() {
            let offset = this.blogNews.length > 0 ? this.blogNews.length : 0;
            BlogService.getAllBlogs(offset).then((response) => {
                if (response.data.success) {
                    this.blogNews = response.data.data.blog_new;
                }
            }).catch((error) => {
                console.log(error);
            })
        },
    },
    created() {
        this.getBlogDetail(this.id);
        this.getAllBlogs();
    }
}
