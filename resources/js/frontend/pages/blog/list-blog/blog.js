import BlogService from "../../../services/blog.service";

export default {
    data() {
        return {
            blogNews: [],
        }
    },
    methods: {
        getAllBlogs() {
            let offset = this.blogNews.length > 0 ? this.blogNews.length : 0;
            BlogService.getAllBlogs(offset).then((response) => {
                if (response.data.success) {
                    this.blogNews = response.data.data.blog_new;
                }
            }).catch((error) => {
                console.log(error);
            })
        },
    },
    created() {
        this.getAllBlogs();
    }
}
