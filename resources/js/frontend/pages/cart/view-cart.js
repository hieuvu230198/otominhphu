import CartService from "../../services/cart.service";
import { PAYMENT } from "../../common/const";
import { email, integer, minLength, maxLength, required } from "vuelidate/lib/validators";
import {messages} from "../../../backend/common/message";

export default {
    data() {
        return {
            messages: messages,
            payments: PAYMENT,
            allCarts: [],
            totalPrice: 0,
            customer: {
                name: '',
                email: '',
                phone: '',
                address: '',
                description: '',
                customer_payment: 1,
            },
            formSubmitted: false,
        }
    },
    methods: {
        addToCart(product, index) {
            const inputQuantity = document.querySelector("#quantity"+index).value;
            this.$emit('updatequantity', {product: product, quantity: inputQuantity});
        },
        removeItemCart(item) {
            this.$emit('removeitemcart', item);
            this.reloadCartData();
        },
        reloadCartData() {
            this.allCarts = JSON.parse(localStorage.getItem('carts')) ? JSON.parse(localStorage.getItem('carts')) : [];
        },
        storeCart() {
            localStorage.setItem('carts', JSON.stringify(this.allCarts));
        },
        handleForm() {
            this.formSubmitted = true;
            this.$v.$touch();
            if (this.$v.$invalid) {
                return false;
            } else {
                if (this.allCarts.length > 0) {
                    this.postCart();
                } else {
                    Swal.fire({
                        text: 'Bạn chưa mua sản phẩm nào!',
                        icon: 'warning',
                    });
                }
            }
        },
        postCart() {
            let data = {
                customer: this.customer,
                carts: JSON.parse(localStorage.getItem('carts')),
                total: this.totalPrice
            };
            CartService.postCart(data).then((response) => {
                this.formSubmitted = false;
                if(response.data.success) {
                    toast.fire({
                        text: 'Đặt hàng thành công!',
                        icon: 'success',
                    });
                    CartService.removeCart();
                    this.reloadCartData();
                    this.$emit('addtocart');
                    this.customer = {
                        customer_payment: 1,
                    };
                }
            }).catch((error) => {
                toast.fire({
                    text: 'Đặt hàng không thành công!',
                    icon: 'success',
                });
            })
        }
    },
    created() {
        this.allCarts = JSON.parse(CartService.getCartLocalStorage()) ? JSON.parse(CartService.getCartLocalStorage()) : [];
    },
    computed: {
        allProduct() {
            return this.allCarts;
        },
        total() {
            let total = 0;
            if (this.allCarts) {
                total = this.allCarts.reduce((total, item) => {
                    return total + item.quantity * item.price;
                }, 0)
            }
            return this.totalPrice = total;
        },
        emailErrorMsg() {
            if(!this.$v.customer.email.required) {
                return  this.messages.EMAIl_REQUIRED;
            }
            if(!this.$v.customer.email.email) {
                return this.messages.EMAIL_INVALID;
            }
            if(this.errors && this.errors.email) {
                return this.errors.email[0];
            }
            return null;
        },
    },
    validations() {
        return {
            customer: {
                name: { required },
                email: { required, email },
                phone: { required, integer, minLength: minLength(9), maxLength: maxLength(11) },
                address: { required },
                description: { required },
            }
        }
    },
}
