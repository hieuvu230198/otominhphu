import HomeService from "../../services/home.service";
import BlogService from "../../services/blog.service";
import CategoryService from "../../services/category.service";

export default {
    props: ['newBlog'],
    data() {
        return {
            categories: {},
            categoryId: '',
            pagination : {
                currentPage: 1,
                pageCount: 1,
            },
            productNews: [],
            productOlds: [],
            blogNews: this.newBlog,
            carts: [],
        }
    },
    methods: {
        discountedPrice(product) {
            return product.pro_price - (product.pro_price *(product.pro_discount)/100)
        },
        getAllCategories() {
            CategoryService.getAllCategories().then((response) => {
                this.categories = response.data.data;
            }).catch((error) => {
                console.log(error);
            })
        },
        getProductNews() {
            let offset = this.productNews.length > 0 ? this.productNews.length : 0;
            return HomeService.getProductNews(offset).then((response) => {
                if (response.data.success) {
                    this.productNews = this.productNews.concat(response.data.data.product_new);
                }
            }).catch((error) => {
                console.log(error);
            })
        },
        getProductOlds() {
            let offset = this.productOlds.length > 0 ? this.productOlds.length : 0;
            return HomeService.getProductOlds(offset).then((response) => {
                if (response.data.success) {
                    this.productOlds = this.productOlds.concat(response.data.data.data);
                }
            }).catch((error) => {
                console.log(error);
            })
        },
        addToCart(product) {
            // check item instance
            let newItemToCartIndex = this.carts.findIndex((item) => {
                return item.id === product.pro_id;
            });
            if (newItemToCartIndex > -1 ) {
                this.carts[newItemToCartIndex].quantity++;
            } else {
                product.quantity = 1;
                if(product.pro_discount == 0){
                    this.carts.push({
                        id: product.pro_id,
                        code: product.pro_code,
                        name: product.pro_name,
                        price: product.pro_price,
                        thumbnail_path: product.thumbnail_path,
                        quantity: product.quantity
                    });
                }else{
                    this.carts.push({
                        id: product.pro_id,
                        code: product.pro_code,
                        name: product.pro_name,
                        price: this.discountedPrice(product),
                        thumbnail_path: product.thumbnail_path,
                        quantity: product.quantity
                    });
                }

            }
            toast.fire({
                text: 'Thêm Sản phẩm thành công!',
                icon: 'success',
            });
            this.storeCart();
            this.$emit('addtocart');
        },
        storeCart() {
            localStorage.setItem('carts', JSON.stringify(this.carts));
        },
    },
    created() {
        this.getAllCategories();
        this.getProductNews();
        this.getProductOlds();
        this.carts = localStorage.getItem('carts') ? JSON.parse(localStorage.getItem('carts')) : [];
    }
}
