import ShoppingService from "../../../services/shopping.service";
import CategoryService from "../../../services/category.service";
import {BASE_URL} from "../../../common/const";

export default {
    data() {
        return {
            categories: {},
            productNews: [],
            carts: [],
            pagination : {
                currentPage: 1,
                pageCount: 1,
            },
            keySearch: '',
            keyCategory: '',
            minPrice: 0 ,
            maxPrice: 50000000
        }
    },
    methods: {
        discountedPrice(product) {
            return product.pro_price - (product.pro_price *(product.pro_discount)/100)
        },
        addToCart(product) {
            // check item instance
            let newItemToCartIndex = this.carts.findIndex((item) => {
                return item.id === product.pro_id;
            });
            if (newItemToCartIndex > -1 ) {
                this.carts[newItemToCartIndex].quantity++;
            } else {
                product.quantity = 1;
                if(product.pro_discount == 0){
                    this.carts.push({
                        id: product.pro_id,
                        code: product.pro_code,
                        name: product.pro_name,
                        price: product.pro_price,
                        thumbnail_path: product.thumbnail_path,
                        quantity: product.quantity
                    });
                }else{
                    this.carts.push({
                        id: product.pro_id,
                        code: product.pro_code,
                        name: product.pro_name,
                        price: this.discountedPrice(product),
                        thumbnail_path: product.thumbnail_path,
                        quantity: product.quantity
                    });
                }
            }
            toast.fire({
                text: 'Thêm Sản phẩm thành công!',
                icon: 'success',
            });
            this.storeCart();
            this.$emit('addtocart');
        },
        storeCart() {
            localStorage.setItem('carts', JSON.stringify(this.carts));
        },
        getProductNews() {
            return ShoppingService.getProductNews(this.pagination.currentPage).then((response) => {
                if (response.data.success) {
                    this.productNews = response.data.data.data;
                    this.pagination.pageCount = response.data.data.last_page;
                    this.pagination.currentPage = response.data.data.current_page;
                }
            }).catch((error) => {
                console.log(error);
            })
        },
        getAllCategories() {
            return CategoryService.getAllCategories().then((response) => {
                this.categories = response.data.data;
            }).catch((error) => {
                console.log(error);
            })
        },
        searchData() {
            let key = this.keySearch;
            let category = this.keyCategory;
            let minPrice = this.minPrice;
            let maxPrice = this.maxPrice;
            if(category === ''){
                location.href = BASE_URL + '/search?key='+ key  + '&minPrice=' + minPrice + '&maxPrice=' + maxPrice + '&category=all';
            } else{
                location.href = BASE_URL + '/search?key='+ key  + '&minPrice=' + minPrice+ '&maxPrice=' + maxPrice + '&category=' + category;
            }
        },

    },
    created() {
        this.getProductNews();
        this.getAllCategories();
        this.carts = localStorage.getItem('carts') ? JSON.parse(localStorage.getItem('carts')) : [];
    }
}

