export const PAYMENT = [
    {text: 'Thanh toán tiền mặt khi nhận hàng (tiền mặt / quẹt thẻ ATM, Visa, Master)', value: 1},
    {text: 'Thanh toán qua chuyển khoản qua tài khoản ngân hàng (khuyên dùng)', value: 2},
    {text: 'Thanh toán qua Ngân Lượng (ATM nội địa, Visa, Master)', value: 3},
    {text: 'Trả góp qua Alepay (Ngân Lượng)', value: 4},
];

export const BASE_URL = process.env.MIX_APP_URL;
export const API_URL = process.env.MIX_API_URL;
export const API_AUTH_URL = process.env.MIX_API_AUTH_URL;
