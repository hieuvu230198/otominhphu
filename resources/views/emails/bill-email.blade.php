@component('mail::message')
# ĐƠN HÀNG ĐÃ ĐƯỢC ĐƯA VÀO CHỜ XỬ LÝ

Kính chào {{ $customer['or_cus_name'] }}
<br>
Cảm ơn Quý khách đã tin tưởng lựa chọn và đồng hành cùng Công ty Minh Phú. Dịch vụ của Quý khách đã được đăng ký thành công.
<br>
Lưu ý: Quý khách kiểm tra lại các thông tin dịch vụ: Gói dịch vụ, thời gian hết hạn, chủ thể đăng ký, phản hồi về minhphuautoparts@gmail.com cho chúng tôi ngay khi phát hiện sai hoặc thiếu sót trong vòng 24 giờ kể từ khi nhận thông báo này.
<br>
Quý khách đọc kỹ Thỏa thuận và Quy định sử dụng dịch vụ và các điều khoản trong Quy định bảo mật để hiểu rõ về các chính sách, điều khoản, quyền lợi và trách nhiệm của Quý khách khi sử dụng dịch vụ tại Minh Phú.
@endcomponent
