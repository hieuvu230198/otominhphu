<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="/admin/home">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0">Manager</h2>
                </a>
            </li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item">
                <a href="/admin/home">
                    <i class="feather icon-home"></i>
                    <span class="menu-title">Dashboard</span>
                </a>
            </li>
            <li class="navigation-header">
                <span>Quản Lý</span>
            </li>
            <li class="nav-item">
                <a href="/admin/manage/products">
                    <i class="feather icon-image"></i>
                    <span class="menu-title">Slider</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/admin/manage/products">
                    <i class="feather icon-layout"></i>
                    <span class="menu-title">Blog</span>
                </a>
            </li>
            @if(Auth::user()->role->name == config('common.role_admin_name'))
            <li class="nav-item">
                <a href="/admin/manage/categories">
                    <i class="feather icon-codepen"></i>
                    <span class="menu-title">Loại sản phẩm</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/admin/manage/products">
                    <i class="feather icon-list"></i>
                    <span class="menu-title">Sản phẩm</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/admin/manage/suppliers">
                    <i class="feather icon-users"></i>
                    <span class="menu-title">Nhà cung cấp</span>
                </a>
            </li>
            @endif
            <li class="nav-item">
                <a href="/admin/manage/bills">
                    <i class="feather icon-file"></i>
                    <span class="menu-title">Hóa đơn</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/admin/manage/comments">
                    <i class="feather icon-eye"></i>
                    <span class="menu-title">Bình luận</span>
                </a>
            </li>
            @if(Auth::user()->role->name == config('common.role_admin_name'))
            <li class="nav-item">
                <a href="/admin/manage/users">
                    <i class="feather icon-settings"></i>
                    <span class="menu-title">Người dùng</span>
                </a>
            </li>
            @endif
            <li class="nav-item">
                <a href="/admin/logout">
                    <i class="feather icon-power"></i>
                    <span class="menu-title">Đăng Xuất</span>
                </a>
            </li>
        </ul>
    </div>
</div>
