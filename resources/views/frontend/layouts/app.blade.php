<!DOCTYPE html>
<html lang="en">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Mobile specific metas
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic page needs
    ============================================ -->
    <title>@yield('title')</title>

    <!-- Favicon
    ============================================ -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/ico/favicon-16x16.png') }}"/>
    <!-- Libs CSS
    ============================================ -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap/css/bootstrap.min.css') }}">
    <link href="{{ asset('assets/css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/js/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/js/owl-carousel/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/themecss/lib.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/js/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/js/minicolors/miniColors.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/js/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" rel="stylesheet">

    <!-- Theme CSS
    ============================================ -->
    <link href="{{ asset('assets/css/themecss/so_searchpro.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/themecss/so_megamenu.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/themecss/so_advanced_search.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/themecss/so-listing-tabs.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/themecss/so-categories.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/themecss/so-newletter-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/themecss/so-latest-blog.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/css/footer/footer2.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/header/header3.css') }}" rel="stylesheet">
    <link id="color_scheme" href="{{ asset('assets/css/home3.css') }}" rel="stylesheet">
    <link id="color_scheme" href="{{ asset('assets/css/theme.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <!-- Google web fonts
    ============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Rubik:300,400,400i,500,600,700' rel='stylesheet' type='text/css'>
    <style type="text/css">
        body {
            font-family: Roboto, Helvetica, Arial, sans-serif;
        }
    </style>

</head>
<body class="common-home res layout-3">
    <div class="wrapper-fluid banners-effect-1">
        <div id="main">
            <header-component ref="headerComponent" @removeitemcart="removeItemCart($event)"></header-component>
            @yield('content')
            <footer-component></footer-component>
        </div>
    </div>

    <!-- Include Libs & Plugins
============================================ -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="{{ asset('assets/js/jquery-2.2.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/owl-carousel/owl.carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/slick-slider/slick.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/themejs/libs.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/unveil/jquery.unveil.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/countdown/jquery.countdown.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/datetimepicker/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/modernizr/modernizr-2.6.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/minicolors/jquery.miniColors.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/lightslider/lightslider.js') }}"></script>

    <!-- Theme files
============================================ -->

    <script type="text/javascript" src="{{ asset('assets/js/themejs/application.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/themejs/homepage.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/themejs/toppanel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/themejs/so_megamenu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/themejs/addtocart.js') }}"></script>
    <script src="{{ mix('js/frontend/main.js') }}" defer></script>
</body>

</html>

