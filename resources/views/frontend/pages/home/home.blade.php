@extends('frontend.layouts.app')

@section('title')
    Trang chu
@endsection
@section('content')
    <!-- Main Container  -->
    <div class="main-container">
        <div id="content">
            <home-component :new-blog="{{ $newBlogs }}" @addToCart="addToCart()" ref="homeComponent"></home-component>
        </div>
    </div>
    <!-- //Main Container -->
@endsection
