@extends('frontend.layouts.app')

@section('title')
    Tìm kiếm
@endsection
@section('content')
        <search-component :products="{{ $products }}" @addToCart="addToCart()"></search-component>
@endsection
