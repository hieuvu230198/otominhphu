@extends('frontend.layouts.app')

@section('title')
    Giỏ Hàng
@endsection
@section('content')
    <cart-component @addToCart="addToCart()" @removeitemcart="removeItemCart($event)" @updatequantity="updateQuantity($event)" ref="cartComponent"></cart-component>
@endsection
