@extends('frontend.layouts.app')

@section('title')
    Danh sách sản phẩm
@endsection
@section('content')
    <product-by-category-component @addToCart="addToCart()"></product-by-category-component>
@endsection
