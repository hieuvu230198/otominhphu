@extends('frontend.layouts.app')

@section('title')
    Shopping
@endsection
@section('content')
    <shopping-component @addToCart="addToCart()"></shopping-component>
@endsection
