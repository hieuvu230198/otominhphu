@extends('frontend.layouts.app')

@section('title')
    Chi tiết sản phẩm
@endsection
@section('content')
    <!-- Main Container  -->
    <div class="main-container container">
        <ul class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i></a></li>
            <li><a href="#">Chi tiết sản phẩm</a></li>
        </ul>
        <div class="row">
            <!--Left Part Start -->
            <aside class="col-sm-4 col-md-3 content-aside" id="column-left" >
                <div class="module category-style">
                    <h3 class="modtitle" style="color: white">MÁY LẠNH NGUYÊN BỘ</h3>
                    <div class="modcontent">
                        <div class="box-category">
                            <ul id="cat_accordion" class="list-group">
                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/1'" class="clearfix">
                                            <span>XE ĐÔNG LẠNH</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/2'" class="clearfix">
                                            <span>XE TẢI VÀ MÁY CÔNG TRÌNH</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/3'" class="clearfix">
                                            <span>XE DU LỊCH VÀ XE KHÁCH</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/4'" class="clearfix">
                                            <span>ĐIỀU HÒA ĐIỆN XE THÙNG KÍN</span>
                                        </a>
                                    </strong>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="module category-style">
                    <h3 class="modtitle" style="color: white" >MÁY NÉN KHÍ-LỐC</h3>
                    <div class="modcontent">
                        <div class="box-category">
                            <ul id="cat_accordion" class="list-group">
                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/5'" class="clearfix">
                                            <span>LỐC 5H11-5H14-507-508</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/6'" class="clearfix">
                                            <span>LỐC 7H13-7H15-708-709</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/7'" class="clearfix">
                                            <span>LỐC HCC-HS15-HS17-HS18</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/8'" class="clearfix">
                                            <span>LỐC DELPHI-V5</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/9'" class="clearfix">
                                            <span>LỐC DENSO-11C-15C-17C-30C</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/10'" class="clearfix">
                                            <span>LỐC XE CON</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/11'" class="clearfix">
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:20px;'>&bull;</span> LỐC XE TOYOTA</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/12'" class="clearfix">
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:20px;'>&bull;</span> LỐC XE HONDA</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/13'" class="clearfix">
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:20px;'>&bull;</span> LỐC XE HYUNDAI</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/14'" class="clearfix">
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:20px;'>&bull;</span> LỐC XE FORD</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/15'" class="clearfix">
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:20px;'>&bull;</span> LỐC XE KIA</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/16'" class="clearfix">
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:20px;'>&bull;</span> LỐC XE DAEWOO</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/17'" class="clearfix">
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:20px;'>&bull;</span> LỐC XE MAZDA</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/18'" class="clearfix">
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:20px;'>&bull;</span> LỐC XE MIT SUBISHI</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/19'" class="clearfix">
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:20px;'>&bull;</span> LỐC XE NISAN</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/20'" class="clearfix">
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:20px;'>&bull;</span> LỐC XE ISUZU</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/21'" class="clearfix">
                                            <span>LỐC XE 16 CHỖ</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/22'" class="clearfix">
                                            <span>LỐC XE 29 CHỖ</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/23'" class="clearfix">
                                            <span>LỐC XE KHÁCH 35-52 CHỖ</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/24'" class="clearfix">
                                            <span>LỐC XE TẢI</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/25'" class="clearfix">
                                            <span>LỐC XE ĐẦU KÉO</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/26'" class="clearfix">
                                            <span>LỐC MÁY XÚC</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/27'" class="clearfix">
                                            <span>LỐC XE ĐÔNG LẠNH</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/28'" class="clearfix">
                                            <span>LỐC KHÔNG PULY</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/29'" class="clearfix">
                                            <span>LỐC ĐÃ QUA SỬ DỤNG</span>
                                        </a>
                                    </strong>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="module category-style">
                    <h3 class="modtitle" style="color: white" >DÀN NÓNG - DÀN LẠNH</h3>
                    <div class="modcontent">
                        <div class="box-category">
                            <ul id="cat_accordion" class="list-group">
                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/30'" class="clearfix">
                                            <span>DÀN NÓNG-LẠNH LIỀN KHỐI</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/31'" class="clearfix">
                                            <span>DÀN NÓNG</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/32'" class="clearfix">
                                            <span>DÀN LẠNH</span>
                                        </a>
                                    </strong>
                                </li>


                            </ul>
                        </div>
                    </div>
                </div>
                <div class="module category-style">
                    <h3 class="modtitle" style="color: white" >QUẠT</h3>
                    <div class="modcontent">
                        <div class="box-category">
                            <ul id="cat_accordion" class="list-group">
                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/33'" class="clearfix">
                                            <span>QUẠT DÀN LẠNH</span>
                                        </a>
                                    </strong>
                                </li>
                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/34'" class="clearfix">
                                            <span>QUẠT DÀN NÓNG</span>
                                        </a>
                                    </strong>
                                </li>
                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/35'" class="clearfix">
                                            <span>MÔ TƠ QUẠT</span>
                                        </a>
                                    </strong>
                                </li>


                            </ul>
                        </div>
                    </div>
                </div>
                <div class="module category-style">
                    <h3 class="modtitle" style="color: white" >LINH PHỤ KIỆN</h3>
                    <div class="modcontent">
                        <div class="box-category">
                            <ul id="cat_accordion" class="list-group">
                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/36'" class="clearfix">
                                            <span>BỘ PULI</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/37'" class="clearfix">
                                            <span>PULI</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/38'" class="clearfix">
                                            <span>MẬT HÍT</span>
                                        </a>
                                    </strong>
                                </li>
                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/39'" class="clearfix">
                                            <span>BÔN ĐIỆN</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/40'" class="clearfix">
                                            <span>VAN TIẾT LƯU</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/41'" class="clearfix">
                                            <span>VAN AN TOÀN ÁP SUẤT</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/42'" class="clearfix">
                                            <span>PHỚT</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/43'" class="clearfix">
                                            <span>PHIM LỌC GA</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/44'" class="clearfix">
                                            <span>NẮP B</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/45'" class="clearfix">
                                            <span>RƠ LE</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/46'" class="clearfix">
                                            <span>GIOĂNG</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/47'" class="clearfix">
                                            <span>ỐNG CAO SU DẪN GA</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/48'" class="clearfix">
                                            <span>KÉT SƯỞI</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/49'" class="clearfix">
                                            <span>VÒNG BI</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/50'" class="clearfix">
                                            <span>CÔNG TẮC-BỘ ĐIỀU KHIỂN</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/51'" class="clearfix">
                                            <span>TRỞ QUẠT</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/52'" class="clearfix">
                                            <span>CO-BÍCH NỔI</span>
                                        </a>
                                    </strong>
                                </li>


                            </ul>
                        </div>
                    </div>
                </div>
                <div class="module category-style">
                    <h3 class="modtitle" style="color: white" >ĐIỆN ĐIỀU HÒA</h3>
                    <div class="modcontent">
                        <div class="box-category">
                            <ul id="cat_accordion" class="list-group">
                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/53'" class="clearfix">
                                            <span>TIẾT CHẾ MÁY PHÁT</span>
                                        </a>
                                    </strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="module category-style">
                    <h3 class="modtitle" style="color: white" >CÔNG CỤ SỬA CHỮA</h3>
                    <div class="modcontent">
                        <div class="box-category">
                            <ul id="cat_accordion" class="list-group">
                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/54'" class="clearfix">
                                            <span>BỘ ĐỒNG HỒ, THỬ KÍN</span>
                                        </a>
                                    </strong>
                                </li>

                                <li class="hadchild">
                                    <strong>
                                        <a :href="'/danh-sach-san-pham/55'" class="clearfix">
                                            <span>GA, DẦU LẠNH</span>
                                        </a>
                                    </strong>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </aside>
            <!--Left Part End -->
            <div id="content" class="col-md-9 col-sm-8">
                <product-detail-component :product-detail="{{ $product }}" @addToCart="addToCart()"></product-detail-component>
                <!-- Related Products -->
                <div class="related titleLine products-list grid module ">
                    <h3 class="modtitle">Sản phẩm liên quan  </h3><br>

                    <div class="releate-products yt-content-slider products-list" data-rtl="no" data-loop="yes" data-autoplay="no" data-autoheight="no" data-autowidth="no" data-delay="4" data-speed="0.6" data-margin="30" data-items_column00="4" data-items_column0="4" data-items_column1="3" data-items_column2="2" data-items_column3="2" data-items_column4="1" data-arrows="yes" data-pagination="no" data-lazyload="yes" data-hoverpause="yes">
                        @foreach($relatedProducts as $relatedProduct)
                            <div class="product-layout product-grid">
                                <div class="product-item-container item--static">
                                    <div class="left-block">
                                        <div class="product-image-container second_img">
                                            <a href="<?php echo $relatedProduct->pro_id; ?>" target="_self" title="Volup tatem accu">
                                                <img src="<?php echo $relatedProduct->thumbnail_path ?>" class="img-1 img-responsive" alt="image1">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <div class="button-group cartinfo--static">

                                            <button type="button" class="wishlist btn-button"><i class="fa fa-heart"></i></button>
                                            <button type="button" class="addToCart">
                                                <a href="<?php echo $relatedProduct->pro_id; ?>"><span>Xem chi tiết</span></a>
                                            </button>
                                            <button type="button" class="compare btn-button"><i class="fa fa-refresh"></i></button>
                                        </div>
                                        <h4><a href="<?php echo $relatedProduct->pro_id; ?>" title="Volup tatem accu" target="_self">{{ $relatedProduct->pro_name }}</a></h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- end Related  Products-->
            </div>
        </div>
    </div>
@endsection
