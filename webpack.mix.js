const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');
mix.js('resources/js/backend/app.js', 'public/js/backend').version();
mix.js('resources/js/frontend/main.js', 'public/js/frontend').version();
mix.copy('resources/app-assets', 'public/app-assets').version();
mix.copy('resources/assets', 'public/assets').version();
mix.disableNotifications();
