<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_detail';
    protected $primaryKey = 'odetail_id';
    protected $guarded = [];
    public $timestamps = true;

    public function orders() {
        return $this->belongsToMany(Order::class);
    }
}
