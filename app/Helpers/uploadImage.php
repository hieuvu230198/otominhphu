<?php
    namespace App\Helper;
    use Illuminate\Support\Facades\Storage;

    class UploadImage {
        public static function uploadImageStorage($file, $storageName) {
            $pathOriginFile = Storage::disk($storageName)->putFile('images', $file);
            return $pathOriginFile;
        }
    }
?>
