<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    public $timestamps = true;
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username' ,'email', 'password', 'phone', 'dateofbirth', 'department', 'role_id', 'avatar', 'thumbnail_path'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_verified_at', 'avatar'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role() {
        return $this->belongsTo(Role::class);
    }

    public static function getAllUsers() {
        $users = User::join('roles', 'roles.id', '=', 'users.role_id')
            ->select('users.*', 'roles.name as roleName')
            ->where('users.is_active', config('common.user_active'))
            ->paginate(config('common.number_paginate'));
        return $users;
    }

    public static function createThumbnailUrl($item) {
        $relativeResourcePath = $item->avatar;
        $existThumbnail = Storage::disk('user')->exists($item->thumbnail_path);
        if($existThumbnail) {
            $item->thumbnail_path = empty($relativeResourcePath) ? null : Storage::disk('user')->url($relativeResourcePath);
        }
        return $item->thumbnail_path;
    }

    public static function getUserProfile($userId) {
        $user = User::findOrFail($userId)->join('roles', 'roles.id', '=', 'users.role_id')
            ->select('users.*', 'roles.name as roleName')
            ->where('users.id', $userId)
            ->first();
        $user->thumbnail_path = self::createThumbnailUrl($user);
        return $user;
    }

    public static function getRole($userId) {
        $user = User::join('roles', 'roles.id', '=', 'users.role_id')
            ->select('users.id', 'users.avatar', 'users.thumbnail_path', 'users.name', 'roles.name as roleName')
            ->where('users.id', $userId)
            ->first();
        $user->thumbnail_path = self::createThumbnailUrl($user);
        return $user;
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
