<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Blog extends Model
{
    protected $table = 'blogs';
    protected $primaryKey = 'blog_id';
    protected $guarded = [];
    public $timestamps = true;

    public static function getAllBlogs() {
        $blogs = Blog::latest()->join('users', 'users.id', '=', 'blogs.blog_user_id')
            ->select('blogs.*', 'users.name as user_name')
            ->where('blogs.is_active', config('common.active'))
            ->paginate(config('common.paginate_product'));
        return $blogs;
    }

    public static function createThumbnailUrl($item) {
        $relativeResourcePath = $item->blog_image;
        $existThumbnail = Storage::disk('thumbnail')->exists($item->thumbnail_path);
        if($existThumbnail) {
            $item->thumbnail_path = empty($item->thumbnail_path) ? null : Storage::disk('thumbnail')->url($item->thumbnail_path);
        } else {
            $item->thumbnail_path = empty($relativeResourcePath) ? null : Storage::disk('blog')->url($relativeResourcePath);
        }
        return $item->thumbnail_path;
    }

    public static function getListBlogs($offset, $limit, $userId = null) {
        $query = Blog::join('users', 'users.id', '=', 'blogs.blog_user_id')
            ->select('blogs.blog_id', 'blogs.blog_image', 'blogs.blog_title','blogs.blog_description',
                'blogs.blog_content','blogs.thumbnail_path', 'blogs.created_at',
                'users.name as author', 'users.id as author_id')
            ->where('blogs.is_active', 1);
        $offset = $offset ? $offset : 0;
        $events = $query->orderBy('created_at', 'desc')
            ->limit($limit)
            ->offset($offset)
            ->get();
        return $events;
    }

    public static function getBlogDetail($blogId) {
        $detailBlog = Blog::join('users', 'users.id', '=', 'blogs.blog_user_id')
            ->select('blogs.blog_id', 'blogs.blog_image', 'blogs.blog_title','blogs.blog_description',
                'blogs.blog_content','blogs.thumbnail_path', 'blogs.created_at',
                'users.name as author', 'users.id as author_id')
            ->where('blogs.is_active', 1)->where('blogs.blog_id',$blogId)->first();
        return $detailBlog;
    }

    public static function getNewsBlog() {
        $blogs = Blog::join('users', 'users.id', '=', 'blogs.blog_user_id')
            ->select('blogs.blog_id', 'blogs.blog_image', 'blogs.blog_title','blogs.blog_description', 'blogs.created_at','users.name as author', 'users.id as author_id')
            ->where('blogs.is_active', 1)->orderBy('created_at', 'desc')->limit(3)->get();
        return $blogs;
    }
}
