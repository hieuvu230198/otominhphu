<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Slider extends Model
{
    protected $table = 'sliders';
    protected $primaryKey = 'sli_id';
    protected $guarded = [];
    public $timestamps = true;

    public static function getAllSliders() {
        $sliders = Slider::latest()->where('is_active', 1)->paginate(config('common.number_paginate'));
        return $sliders;
    }

    public static function createThumbnailUrl($item) {
        $relativeResourcePath = $item->sli_image;
        $existThumbnail = Storage::disk('thumbnail')->exists($item->thumbnail_path);
        if($existThumbnail) {
            $item->thumbnail_path = empty($item->thumbnail_path) ? null : Storage::disk('thumbnail')->url($item->thumbnail_path);
        } else {
            $item->thumbnail_path = empty($relativeResourcePath) ? null : Storage::disk('slider')->url($relativeResourcePath);
        }
        return $item->thumbnail_path;
    }

    public static function getListSliders() {
        $slides = Slider::latest()
            ->select('sliders.sli_id', 'sliders.sli_name', 'sliders.sli_image','sliders.thumbnail_path', 'sliders.created_at')
            ->where('sliders.is_active', 1)->orderBy('created_at', 'desc')->limit(5)->get();
        return $slides;
    }
}
