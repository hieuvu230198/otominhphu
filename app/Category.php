<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'cate_id';
    protected $guarded = [];
    public $timestamps = true;

    public static function getAllCategories() {
        $categories = Category::where('is_active', 1)->get(['cate_id', 'name', 'description']);
        return $categories;
    }
}
