<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'pro_id';
    protected $guarded = [];
    public $timestamps = true;


    public static function getAllProducts() {
        $product = Product::latest()->join('categories', 'categories.cate_id', '=', 'products.pro_category_id')
            ->join('suppliers', 'suppliers.supp_id', '=', 'products.pro_supplier_id')
            ->select('products.*', 'categories.name as category', 'suppliers.supp_address as supp_address')
            ->where('products.pro_active', 1)
            ->paginate(config('common.number_paginate'));
        return $product;
    }
    public static function getListProducts($offset, $limit) {
        $query = Product::join('categories', 'categories.cate_id', '=', 'products.pro_category_id')
            ->select('products.pro_id', 'products.pro_brand', 'products.pro_image', 'products.pro_code','products.pro_name',
                'products.pro_price', 'products.pro_description','products.thumbnail_path', 'products.created_at','products.pro_discount',
                'categories.name as category', 'categories.cate_id as category_id')
            ->where('products.pro_active', 1);
        $offset = $offset ? $offset : 0;
        $events = $query->orderBy('created_at', 'desc')
            ->limit($limit)
            ->offset($offset)
            ->get();
        return $events;
    }
    public static function getNewProducts() {
        $newProducts = Product::latest()
            ->join('categories', 'categories.cate_id', '=', 'products.pro_category_id')
            ->select('products.pro_id', 'products.pro_brand', 'products.pro_image', 'products.pro_code','products.pro_name',
                'products.pro_price', 'products.pro_description','products.thumbnail_path', 'products.created_at','products.pro_discount',
                'categories.name as category', 'categories.cate_id as category_id')
            ->where('products.pro_active', 1)
            ->paginate(config('common.number_paginate'));
        return $newProducts;
    }
    public static function getOldProducts() {
        $oldProduct = Product::join('categories', 'categories.cate_id', '=', 'products.pro_category_id')
            ->select('products.pro_id', 'products.pro_brand', 'products.pro_image', 'products.pro_code','products.pro_name',
                'products.pro_price', 'products.pro_description','products.thumbnail_path', 'products.created_at','products.pro_discount',
                'categories.name as category', 'categories.cate_id as category_id')
            ->where('products.pro_active', 1)
            ->orderBy('created_at', 'asc')
            ->paginate(config('common.paginate_product'));
        return $oldProduct;
    }

    public static function getDetailProduct($productId) {
        $product = Product::join('categories', 'categories.cate_id', '=', 'products.pro_category_id')
            ->select('products.pro_id', 'products.pro_brand', 'products.pro_image', 'products.pro_code','products.pro_name', 'pro_wattage', 'pro_height_weight',
                'products.pro_price', 'products.pro_description','products.thumbnail_path', 'products.created_at','products.pro_discount',
                'categories.name as category', 'categories.cate_id as category_id')
            ->where('products.pro_id', $productId)
            ->where('products.pro_active', 1)
            ->first();
        return $product;
    }

    //SELECT * FROM `products` WHERE `pro_category_id` = $categoryId
    public static function getProductByCategory($categoryId) {
        $product = Product::latest()->join('categories', 'categories.cate_id', '=', 'products.pro_category_id')
            ->join('suppliers', 'suppliers.supp_id', '=', 'products.pro_supplier_id')
            ->select('products.*', 'categories.name as category', 'suppliers.supp_address as supp_address')
            ->where('products.pro_active', 1)
            ->where('pro_category_id',$categoryId)
            ->paginate(config('common.number_paginate'));
        return $product;
    }

    public static function createThumbnailUrl($item) {
        $relativeResourcePath = $item->pro_image;
        $existThumbnail = Storage::disk('product')->exists($item->thumbnail_path);
        if($existThumbnail) {
            $item->thumbnail_path = empty($relativeResourcePath) ? null : Storage::disk('product')->url($relativeResourcePath);
        }
        return $item->thumbnail_path;
    }

}
