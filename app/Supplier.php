<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Supplier extends Model
{
    protected $table = 'suppliers';
    protected $primaryKey = 'supp_id';
    protected $guarded = [];
    public $timestamps = true;

    protected $hidden = [
        'is_active'
    ];

    public static function getAllSuppliers() {
        $suppliers = Supplier::where('is_active', 1)->get();
        return $suppliers;
    }

    public static function createThumbnailUrl($item) {
        $relativeResourcePath = $item->supp_logo;
        $existThumbnail = Storage::disk('supplier')->exists($item->thumbnail_path);
        if($existThumbnail) {
            $item->thumbnail_path = empty($relativeResourcePath) ? null : Storage::disk('supplier')->url($relativeResourcePath);
        }
        return $item->thumbnail_path;
    }
}
