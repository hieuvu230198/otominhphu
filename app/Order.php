<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'or_id';
    protected $guarded = [];
    public $timestamps = true;

    public static function getAllBills() {
        $bills = Order::latest()->where('is_active',1)
            ->paginate(config('common.number_paginate'));
        return $bills;
    }

    public static function getBillDetail($billId) {
        $bill = Order::findOrFail($billId)
            ->join('order_detail', 'order_detail.odetail_order_id', '=', 'orders.or_id')
            ->join('products', 'products.pro_id', '=', 'order_detail.odetail_product_id')
            ->select('order_detail.*', 'products.pro_code as pro_code', 'products.pro_name as pro_name')
            ->where('orders.or_id', $billId)
            ->get();
        return $bill;
    }
}
