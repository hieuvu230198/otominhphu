<?php

namespace App\Mail\backend;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BillMail extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;
    public $billDetail;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer, $billDetail)
    {
        $this->customer = $customer;
        $this->billDetail = $billDetail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $customer = $this->customer;
        $billDetail = $this->billDetail;
        return $this->markdown('emails.bill-email', compact('customer', 'billDetail'));
    }
}
