<?php

namespace App\Http\Middleware\backend;

use Closure;
use Illuminate\Support\Facades\Auth;

class checkAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && (Auth::user()->role_id == config('common.role_admin'))) {
            return $next($request);
        } else {
            return abort(401);
        }
    }
}
