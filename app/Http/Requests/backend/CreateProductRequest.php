<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pro_code' => 'required|string|max:191',
            'pro_name' => 'required|string|max:191',
            'pro_category_id' => 'required',
//            'pro_image' => 'required|string|max:191',
            'pro_unit' => 'string|max:191',
            'pro_upro_unit_price' => 'string|max:191',
            'propro_discount' => 'string|max:191',
            'pro_pepro_performance' => 'string|max:191',
            'pro_tpro_technology' => 'string|max:191',
            'propro_quantity' => 'string|max:191',
            'pro_depro_description' => 'string|max:191',
            'pro_supro_supplier_id' => 'string|max:191',
            'pro_diameter' => 'string|max:191',
            'pro_status' => 'string|max:191',
            'pro_active' => 'string|max:191',
        ];
    }
}
