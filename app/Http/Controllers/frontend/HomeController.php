<?php

namespace App\Http\Controllers\frontend;

use App\Blog;
use App\Product;
use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    public function index() {
        $slides = $this->getNewSlider();
        $newBlogs = $this->getNewBlogs();
        return view('frontend.pages.home.home', compact('slides', 'newBlogs'));
    }

    public function getNewSlider() {
        $slides = Slider::getListSliders();
        $slides->map(function ($item) {
            $item = Slider::createThumbnailUrl($item);
            return $item;
        });
        return $slides;
    }

    public function getNewBlogs() {
        $blogs = Blog::getNewsBlog();
        $blogs->map(function ($item) {
            $item->create_at = $item->created_at->diffForHumans();
            $item = Blog::createThumbnailUrl($item);
            return $item;
        });
        return $blogs;
    }

    public function searchProduct() {
        $key = $_GET['key'];
        $category = $_GET['category'];
        if (!isset($_GET['minPrice'])) {
            if($category == 'all'){
                $products = Product::where(function($query) use ($key){
                    $query->orwhere('pro_code', 'LIKE', "%{$key}%");
                    $query->orWhere('pro_name', 'LIKE', "%{$key}%")->get();
                })
                    ->get();

                $products->map(function ($item) {
                    $item = Product::createThumbnailUrl($item);
                    return $item;
                });
            } else {
                $products = Product::where('products.pro_category_id',$category)
                    ->where(function($query) use ($key){
                        $query->orwhere('pro_code', 'LIKE', "%{$key}%");
                        $query->orWhere('pro_name', 'LIKE', "%{$key}%");
                    })
                    ->get();

                $products->map(function ($item) {
                    $item = Product::createThumbnailUrl($item);
                    return $item;
                });
            }
        } else{
            $minPrice = $_GET['minPrice'];
            $maxPrice = $_GET['maxPrice'];
            if($category == 'all'){
                $products = Product::where('pro_price', '>=', $minPrice)
                    ->where('pro_price', '<=', $maxPrice)
                    ->where(function($query) use ($key){
                        $query->orwhere('pro_code', 'LIKE', "%{$key}%");
                        $query->orWhere('pro_name', 'LIKE', "%{$key}%")->get();
                    })
                    ->get();

                $products->map(function ($item) {
                    $item = Product::createThumbnailUrl($item);
                    return $item;
                });
            } else {
                $products = Product::where('products.pro_category_id',$category)
                    ->where(function($query) use ($minPrice, $maxPrice){
                        $query->where('pro_price', '>=', $minPrice);
                        $query->where('pro_price', '<=', $maxPrice)->get();
                    })
                    ->where(function($query) use ($key){
                        $query->orwhere('pro_code', 'LIKE', "%{$key}%");
                        $query->orWhere('pro_name', 'LIKE', "%{$key}%")->get();
                    })
                    ->get();

                $products->map(function ($item) {
                    $item = Product::createThumbnailUrl($item);
                    return $item;
                });
            }
        }
        return view('frontend.pages.search.index', compact('products'));
    }
}
