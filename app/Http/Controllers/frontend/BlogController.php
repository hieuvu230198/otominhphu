<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function index() {
        return view('frontend.pages.blog.index');
    }

    public function detailBlog() {
        return view('frontend.pages.blog.detailBlog');
    }

}
