<?php

namespace App\Http\Controllers\frontend;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShoppingController extends Controller
{
    public function index() {
        return view('frontend.pages.shopping.index');
    }

    public function productDetail($productId) {
        $categories = Category::getAllCategories();
        $product = Product::getDetailProduct($productId);
        $product->thumbnail_path = Product::createThumbnailUrl($product);
        $relatedProducts = Product::where('pro_category_id', $product->category_id)->limit(5)->get();
        $relatedProducts->map(function ($item) {
            $item = Product::createThumbnailUrl($item);
            return $item;
        });
        $productNews = Product::orderBy('created_at', 'DESC')->limit(8)->get();
        $productNews->map(function ($item) {
            $item = Product::createThumbnailUrl($item);
            return $item;
        });
        return view('frontend.pages.product.product-detail', compact('product', 'categories', 'relatedProducts', 'productNews'));
    }

    public function productByCategory($categoryId) {
        $productByCate = Product::getProductByCategory($categoryId);
        $productByCate->map(function ($item) {
            $item = Product::createThumbnailUrl($item);
            return $item;
        });
        return view('frontend.pages.category.product-by-category', compact('productByCate'));
    }

}
