<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OrderDetail;
use App\Order;
class DownLoadBillController extends Controller
{
    public function index(Request $request) {
        $fileType = \PHPExcel_IOFactory::identify('TemplateExcel/template.xlsx');
        $objReader = \PHPExcel_IOFactory::createReader($fileType);
        $objPHPExcel = $objReader->load('TemplateExcel/template.xlsx');
        $billId = $request->all()['id'];
        $bookData = $this->getDataBill($billId);
        $this->addDataToExcelFile($objPHPExcel->setActiveSheetIndex(0), $bookData,$objPHPExcel);
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $fileName = time() . '_bill_info.xlsx';
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $objWriter->save('php://output');
    }
    public function getDataBill($billId) {
        $arrInfoBill = [];
        $billByUser = Order::where('or_id', $billId)->first();
        $billDetails = Order::getBillDetail($billId);
        $arrInfoBill['user'] = $billByUser;
        $arrInfoBill['billDetail'] = $billDetails;
        return $arrInfoBill;
    }
    private function addDataToExcelFile ($setCell, $bookData,$objPHPExcel){
        $setCell->setCellValue('E7', $bookData['user']['or_cus_address']);
        $setCell->setCellValue('E6', $bookData['user']['or_cus_name']);
        $setCell->setCellValue('E8', $bookData['user']['or_cus_phone']);
        $setCell->setCellValue('D3', $bookData['user']['or_shipped_date']);
        $index = 11;
        $totalMoney = 0;
        $currency = ' VND';
        foreach($bookData['billDetail'] as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$index.':'.'J'.$index);
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('K'.$index.':'.'L'.$index);
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('M'.$index.':'.'O'.$index);
            $setCell->setCellValue('D2', $value['odetail_order_id']);
            $setCell->setCellValue('C'.$index, $key + 1);
            $setCell->setCellValue('D'.$index, $value['pro_name']);
            $setCell->setCellValue('K'.$index, $value['odetail_quantity']);
            $setCell->setCellValue('M'.$index, number_format($value['odetail_unit_price']) .$currency);
            $setCell->setCellValue('P'.$index, number_format($value['odetail_total_money']).$currency);
            $index ++;
            $totalMoney += $value['odetail_total_money'];
        }
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('M'.($index+2).':'.'O'.($index+2));
        $setCell->setCellValue('M'.($index + 2), "Thanh Toán : ");
        $setCell->setCellValue('P'.($index + 2), number_format($totalMoney) .$currency);

        $setCell->getStyle("C10:P" .($index - 1))->applyFromArray(array(
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                    'size' => 1,
                ),
                'inside' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                    'size' => 1,
                ),
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        ));
        return $this;
    }
}
