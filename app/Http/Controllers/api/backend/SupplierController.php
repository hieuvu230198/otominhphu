<?php

namespace App\Http\Controllers\api\backend;

use App\Helper\UploadImage;
use App\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class SupplierController extends Controller
{
    protected $uploadImage;
    public function __construct() {
        $this->middleware('jwt.auth');
        $this->uploadImage = new UploadImage();
    }
    public function getAllSuppliers() {
        $suppliers = Supplier::getAllSuppliers();
        $suppliers->map(function ($item) {
            $item = Supplier::createThumbnailUrl($item);
            return $item;
        });
        return response()->apiRet($suppliers);
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();
            $avatarPath = '';
            if ($request->hasFile('supp_logo')) {
                $avatarPath = $this->uploadImage->uploadImageStorage($request->file('supp_logo'), "supplier");
            }
            Supplier::create([
                'supp_name' => $request->supp_name,
                'supp_address' => $request->supp_address,
                'supp_phone' => $request->supp_phone,
                'supp_email' => $request->supp_email,
                'supp_logo' => $avatarPath,
                'thumbnail_path' => $avatarPath,
            ]);
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
    public function update(Request $request, $supplierId) {
        $supplier = Supplier::find($supplierId);
        $this->validate($request,[
            'supp_name' => 'required',
        ]);
        try {
            DB::beginTransaction();
            $avatarPath = $supplier->supp_logo;
            if ($request->hasFile('supp_logo')) {
                if(Storage::disk('supplier')->exists($supplier->supp_logo)) {
                    Storage::disk('supplier')->delete($supplier->supp_logo);
                }
                $avatarPath = $this->uploadImage->uploadImageStorage($request->file('supp_logo'), "supplier");
            }
            $supplier->update([
                'supp_name' => $request->supp_name,
                'supp_address' => $request->supp_address,
                'supp_phone' => $request->supp_phone,
                'supp_email' => $request->supp_email,
                'supp_logo' => $avatarPath,
                'thumbnail_path' => $avatarPath,
            ]);
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
    public function deleteSupplier($supplierId) {
        if (auth('api')->user()->role->name !== 'admin') {
            if (auth('api')->user()->role->name !== 'admin') {
                return response()->unauthorizedError();
            }
            return response()->internalError();
        }
        if(empty($supplierId)) {
            return response()->apiValidateError("User id cannot be empty");
        }
        try {
            DB::beginTransaction();
            $supplier = Supplier::find($supplierId);
            $supplier->is_active = config('common.not_active');
            $supplier->save();
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
    public function search(Request $request) {
        try {
            $suppName = $request->key;
            $slides = Supplier::where('supp_name', 'LIKE', "%{$suppName}%")->paginate(config('common.number_paginate'));
            $slides->map(function ($item) {
                $item = Supplier::createThumbnailUrl($item);
                return $item;
            });
            return response()->apiRet($slides);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
}
