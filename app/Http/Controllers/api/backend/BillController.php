<?php

namespace App\Http\Controllers\api\backend;

use App\Mail\backend\BillMail;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class BillController extends Controller
{
    public function getAllBills() {
        $bills = Order::getAllBills();
        return response()->apiRet($bills);
    }

    public function getBillDetail($billId) {
        $bill = Order::where('or_id', $billId)->first();
        $billDetails = Order::getBillDetail($billId);
        return response()->apiRet(['billDetail' => $billDetails, 'bill' => $bill]);
    }

    public function changeStatus(Request $request) {
        $billId = $request->billId;
        $status = $request->status;
        try {
            DB::beginTransaction();
            $bill = Order::where('or_id', $billId)->first();
            $bill->or_status_id = $status;
            $bill->or_user_id = auth('api')->user()->id;
            $billDetail = Order::getBillDetail($bill->or_id);
            // get odetail_product_id and odetail_quantity
            $this->decrementQuantityProduct($billDetail);
            $bill->save();
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
    public function decrementQuantityProduct($billDetail) {
        // get odetail_product_id and odetail_quantity
        foreach ($billDetail as $productDetail) {
            $product  = Product::where('pro_id', $productDetail->odetail_product_id)->first();
            if ($product->pro_quantity == 0) {
                $product->pro_quantity = 0;
                $product->save();
                return true;
            } else if ($productDetail->odetail_quantity < $product->pro_quantity) {
                $product->pro_quantity = (int)$product->pro_quantity - (int)$productDetail->odetail_quantity;
                $product->save();
                return true;
            } else {
                return response()->apiValidateError("Product no longer available");
            }
        }
    }

    public function deleteBill($billId) {
        if (auth('api')->user()->role->name !== 'admin') {
            if (auth('api')->user()->role->name !== 'admin') {
                return response()->unauthorizedError();
            }
            return response()->internalError();
        }
        try {
            DB::beginTransaction();
            if(empty($billId)) {
                return response()->apiValidateError("Bill id cannot be empty");
            }
            $bill = Order::findOrFail($billId);
            $bill->is_active = config('common.user_not_active');
            $bill->save();
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }

    public function search(Request $request) {
        try {
            $key = $request->key;
            $bills = Order::where('or_cus_name', 'LIKE', "%{$key}%")
                ->orWhere('or_cus_email', 'LIKE', "%{$key}%")
                ->orWhere('or_cus_phone', 'LIKE', "%{$key}%")
                ->orWhere('or_id', 'LIKE', "%{$key}%")
                ->paginate(config('common.number_paginate'));
            return response()->apiRet($bills);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }

    public function sendMail(Request $request) {
        $customer = $request->customer;
        $billDetail = $request->billDetail;
        Mail::to($customer['or_cus_email'])->send(new BillMail($customer, $billDetail));
    }
}
