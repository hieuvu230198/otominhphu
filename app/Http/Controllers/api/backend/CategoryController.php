<?php

namespace App\Http\Controllers\api\backend;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller
{
    public function getAllCategories() {
        $categories = Category::getAllCategories();
        return response()->apiRet($categories);
    }
    public function store(Request $request) {
        try {
            DB::beginTransaction();
            Category::create([
                'name' => $request->name,
                'description' => $request->description,
            ]);
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
    public function update(Request $request, $categoryId) {
        $category = Category::find($categoryId);
        $this->validate($request,[
            'name' => 'required',
        ]);
        try {
            DB::beginTransaction();
            $category->update([
                'name' => $request->name,
                'description' => $request->description,
            ]);
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
    public function deleteCategory($categoryId) {
        if (auth('api')->user()->role->name !== 'admin') {
            if (auth('api')->user()->role->name !== 'admin') {
                return response()->unauthorizedError();
            }
            return response()->internalError();
        }
        if(empty($categoryId)) {
            return response()->apiValidateError("User id cannot be empty");
        }
        try {
            DB::beginTransaction();
            $category = Category::find($categoryId);
            $category->is_active = config('common.not_active');
            $category->save();
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }

    public function search(Request $request) {
        try {
            $nameCategory = $request->key;
            $categoriess = Category::where('name', 'LIKE', "%{$nameCategory}%")->get();
            return response()->apiRet($categoriess);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
}
