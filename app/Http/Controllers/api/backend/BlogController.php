<?php

namespace App\Http\Controllers\api\backend;

use App\Blog;
use App\Helper\UploadImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    protected $uploadImage;
    public function __construct() {
        $this->middleware('jwt.auth');
        $this->uploadImage = new UploadImage();
    }

    public function getAllBlogs() {
        $blogs = Blog::getAllBlogs();
        $blogs->map(function ($item) {
            $item = Blog::createThumbnailUrl($item);
            return $item;
        });
        return response()->apiRet($blogs);
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();
            $avatarPath = '';
            if ($request->hasFile('blog_image')) {
                $avatarPath = $this->uploadImage->uploadImageStorage($request->file('blog_image'), "blog");
            }
            Blog::create([
                'blog_title' => $request->blog_title,
                'blog_description' => $request->blog_description,
                'blog_content' => $request->blog_content,
                'blog_user_id' => $request->blog_user_id,
                'blog_image' => $avatarPath,
                'thumbnail_path' => $avatarPath,
            ]);
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }

    public function update(Request $request, $blogId) {
        $blog = Blog::find($blogId);
        $this->validate($request,[
            'blog_title' => 'required',
        ]);
        try {
            DB::beginTransaction();
            $avatarPath = $blog->blog_image;
            if ($request->hasFile('blog_image')) {
                if(Storage::disk('blog')->exists($blog->blog_image)) {
                    Storage::disk('blog')->delete($blog->blog_image);
                }
                $avatarPath = $this->uploadImage->uploadImageStorage($request->file('blog_image'), "blog");
            }
            $blog->update([
                'blog_title' => $request->blog_title,
                'blog_description' => $request->blog_description,
                'blog_content' => $request->blog_content,
                'blog_user_id' => auth('api')->user()->id,
                'blog_image' => $avatarPath,
                'thumbnail_path' => $avatarPath,
            ]);
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }

    public function deleteBlog($blogId) {
        if (auth('api')->user()->role->name !== 'admin') {
            if (auth('api')->user()->role->name !== 'admin') {
                return response()->unauthorizedError();
            }
            return response()->internalError();
        }
        if(empty($blogId)) {
            return response()->apiValidateError("User id cannot be empty");
        }
        try {
            DB::beginTransaction();
            $blog = Blog::find($blogId);
            $blog->is_active = config('common.not_active');
            $blog->save();
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }

    public function search(Request $request) {
        try {
            $titleBlog = $request->key;
            $slides = Blog::where('blog_title', 'LIKE', "%{$titleBlog}%")->paginate(config('common.number_paginate'));
            $slides->map(function ($item) {
                $item = Blog::createThumbnailUrl($item);
                return $item;
            });
            return response()->apiRet($slides);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
}
