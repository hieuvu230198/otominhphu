<?php

namespace App\Http\Controllers\api\backend;

use App\Helper\UploadImage;
use App\Http\Requests\backend\CreateProductRequest;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    protected $uploadImage;
    public function __construct() {
        $this->middleware('jwt.auth');
        $this->uploadImage = new UploadImage();
    }
    public function getAllProducts() {
        $product = Product::getAllProducts();
        $product->map(function ($item) {
            $item = Product::createThumbnailUrl($item);
            return $item;
        });
        return response()->apiRet($product);
    }
    public function deleteProduct($productId) {
        if (auth('api')->user()->role->name !== 'admin') {
            if (auth('api')->user()->role->name !== 'admin') {
                return response()->unauthorizedError();
            }
            return response()->internalError();
        }
        try {
            DB::beginTransaction();
            if(empty($productId)) {
                return response()->apiValidateError("Product id cannot be empty");
            }
            $product = Product::findOrFail($productId);
            $product->pro_active = 0;
            $product->save();
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
    public function store(Request $request) {
        try {
            DB::beginTransaction();
            $productImagePath = '';
            if ($request->hasFile('pro_image')) {
                $productImagePath = $this->uploadImage->uploadImageStorage($request->file('pro_image'), "product");
            }
            Product::create([
                'pro_code' => $request->pro_code ? $request->pro_code : Str::uuid(),
                'pro_name' => $request->pro_name,
                'pro_category_id' => $request->pro_category_id,
                'pro_discount' => $request->pro_discount,
                'pro_price' => $request->pro_price,
                'pro_brand' => $request->pro_brand,
                'pro_quantity' => $request->pro_quantity,
                'pro_wattage' => $request->pro_wattage,
                'pro_height_weight' => $request->pro_height_weight,
                'pro_description' => $request->pro_description,
                'pro_supplier_id' => $request->pro_supplier_id,
                'pro_image' => $productImagePath,
                'thumbnail_path' => $productImagePath,
            ]);
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
    public function update(Request $request, $productId) {
        $product = Product::findOrFail($productId);
        try {
            DB::beginTransaction();
            $productImagePath = $product->pro_image;
            if ($request->hasFile('pro_image')) {
                if(Storage::disk('product')->exists($product->pro_image)) {
                    Storage::disk('product')->delete($product->pro_image);
                }
                $productImagePath = $this->uploadImage->uploadImageStorage($request->file('pro_image'), "product");
            }
            $product->update([
                'pro_code' => $request->pro_code ? $request->pro_code : Str::uuid(),
                'pro_name' => $request->pro_name,
                'pro_category_id' => $request->pro_category_id,
                'pro_discount' => $request->pro_discount,
                'pro_price' => $request->pro_price,
                'pro_brand' => $request->pro_brand,
                'pro_quantity' => $request->pro_quantity,
                'pro_wattage' => $request->pro_wattage,
                'pro_height_weight' => $request->pro_height_weight,
                'pro_description' => $request->pro_description,
                'pro_supplier_id' => $request->pro_supplier_id,
                'pro_image' => $productImagePath,
                'thumbnail_path' => $productImagePath,
            ]);
            $product->save();
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }

    public function search(Request $request) {
        try {
            $key = $request->key;
            $users = Product::where('pro_code', 'LIKE', "%{$key}%")->orWhere('pro_name', 'LIKE', "%{$key}%")->paginate(config('common.number_paginate'));
            $users->map(function ($item) {
                $item = Product::createThumbnailUrl($item);
                return $item;
            });
            return response()->apiRet($users);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
}
