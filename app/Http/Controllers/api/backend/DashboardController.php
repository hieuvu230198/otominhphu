<?php

namespace App\Http\Controllers\api\backend;

use App\User;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Order;
use App\OrderDetail;
use App\Supplier;

class DashboardController extends Controller
{
    public function __construct() {
        $this->middleware('jwt.auth');
    }
    public function getDataDashBoard() {
        $arrData = [];
        $user = User::where('is_active','1')->count();
        $product = Product::where('pro_active','1')->count();
        $supplier = Supplier::where('is_active','1')->count();
        $arrData["countUser"] = $user;
        $arrData["countProduct"] = $product;
        $arrData["countSuppliers"] =  $supplier;
        $now = Carbon::now();
        $weekStartDate = $now->startOfWeek()->format('Y-m-d H:i:s');
        $weekEndDate = $now->endOfWeek()->format('Y-m-d H:i:s');

        $order = Order::select(
            ['orders.or_id','orders.or_user_id as user_id','orders.created_at',
                DB::raw('sum((select count(order_detail.odetail_product_id) FROM order_detail where orders.or_id = order_detail.odetail_order_id)) as countProducts'),
                DB::raw('(SELECT users.name FROM users WHERE orders.or_user_id = users.id) as userName')
            ]
        )->where('orders.is_active',1)->whereBetween('orders.created_at', [$weekStartDate, $weekEndDate])
            ->groupBy('user_id')
            ->orderBy('countProducts','desc')->get();
        $strName = "";
        if($order->isEmpty()){
            $strName = "Chưa có nhân viên nào , ";
        } else {
            $countProduct = $order[0]['countProducts'];
            foreach($order as $key => $value){
                if($countProduct ==  $value['countProducts']){
                    $strName .= $value['userName'].' , ';
                }
            }
        }
        $arrData['userPerfect'] = substr($strName, 0, strlen($strName) - 2);

        //SELECT `or_cus_name`,`or_cus_phone`, SUM(`or_total`) as Amount FROM `orders` GROUP BY `or_cus_phone` ORDER BY Amount DESC LIMIT 5

        $countMoney = Order::select('or_cus_phone', 'or_cus_name',
            DB::raw('SUM(or_total) as countMoneys')
            )
        ->where('orders.is_active',1)
            ->groupBy('or_cus_phone')
            ->orderBy('countMoneys','desc')
            ->limit('5')
            ->get();

        $arrData['topCustomer'] =  $countMoney;
        //

        return response()->apiRet($arrData);
    }
}
