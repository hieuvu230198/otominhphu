<?php

namespace App\Http\Controllers\api\backend;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function getAllRoles() {
        $roles = Role::all();
        return response()->apiRet($roles);
    }
}
