<?php

namespace App\Http\Controllers\api\backend;

use App\OrderDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BillDetailController extends Controller
{
    public function getAllBillDetails() {
        $billDetails = OrderDetail::all();
        return response()->apiRet($billDetails);
    }
}
