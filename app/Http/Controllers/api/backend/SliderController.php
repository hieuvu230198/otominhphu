<?php

namespace App\Http\Controllers\api\backend;

use App\Slider;
use App\Helper\UploadImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    protected $uploadImage;
    public function __construct() {
        $this->middleware('jwt.auth');
        $this->uploadImage = new UploadImage();
    }

    public function getAllSliders() {
        $sliders = Slider::getAllSliders();
        $sliders->map(function ($item) {
            $item = Slider::createThumbnailUrl($item);
            return $item;
        });
        return response()->apiRet($sliders);
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();
            $avatarPath = '';
            if ($request->hasFile('sli_image')) {
                $avatarPath = $this->uploadImage->uploadImageStorage($request->file('sli_image'), "slider");
            }
            Slider::create([
                'sli_name' => $request->sli_name,
                'sli_image' => $avatarPath,
                'thumbnail_path' => $avatarPath,
            ]);
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }

    public function update(Request $request, $sliderId) {
        $slider = Slider::find($sliderId);
        $this->validate($request,[
            'sli_name' => 'required',
        ]);
        try {
            DB::beginTransaction();
            $avatarPath = $slider->sli_image;
            if ($request->hasFile('sli_image')) {
                if(Storage::disk('slider')->exists($slider->sli_image)) {
                    Storage::disk('slider')->delete($slider->sli_image);
                }
                $avatarPath = $this->uploadImage->uploadImageStorage($request->file('sli_image'), "slider");
            }
            $slider->update([
                'sli_name' => $request->sli_name,
                'sli_image' => $avatarPath,
                'thumbnail_path' => $avatarPath,
            ]);
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }

    public function deleteSlider($sliderId) {
        if (auth('api')->user()->role->name !== 'admin') {
            if (auth('api')->user()->role->name !== 'admin') {
                return response()->unauthorizedError();
            }
            return response()->internalError();
        }
        if(empty($sliderId)) {
            return response()->apiValidateError("User id cannot be empty");
        }
        try {
            DB::beginTransaction();
            $slider = Slider::find($sliderId);
            $slider->is_active = config('common.not_active');
            $slider->save();
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }

    public function search(Request $request) {
        try {
            $nameSlide = $request->key;
            $slides = Slider::where('sli_name', 'LIKE', "%{$nameSlide}%")->paginate(config('common.number_paginate'));
            $slides->map(function ($item) {
                $item = Slider::createThumbnailUrl($item);
                return $item;
            });
            return response()->apiRet($slides);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
}
