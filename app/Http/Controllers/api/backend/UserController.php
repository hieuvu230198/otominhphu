<?php

namespace App\Http\Controllers\api\backend;

use App\User;
use App\Helper\UploadImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\backend\CreateUserRequest;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    protected $uploadImage;
    public function __construct() {
        $this->middleware('jwt.auth');
        $this->uploadImage = new UploadImage();
    }
    public function getAllUsers() {
        $users = User::getAllUsers();
        $users->map(function ($item) {
            $item = User::createThumbnailUrl($item);
            return $item;
        });
        return response()->apiRet($users);
    }
    public function getUserWithId($userId) {
        $user = User::getUserProfile($userId);
        return response()->apiRet($user);
    }
    public function store(CreateUserRequest $request) {
        try {
            DB::beginTransaction();
            $avatarPath = '';
            if ($request->hasFile('avatar')) {
                $avatarPath = $this->uploadImage->uploadImageStorage($request->file('avatar'), "user");
            }
            User::create([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'phone' => $request->phone,
                'department' => $request->department,
                'dateofbirth' => $request->dateOfBirth,
                'role_id' => $request->role_id ? $request->role_id : config('common.role_user'),
                'avatar' => $avatarPath,
                'thumbnail_path' => $avatarPath,
            ]);
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
    public function update(Request $request, $userId) {
        $user = User::findOrFail($userId);
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
        ]);
        try {
            DB::beginTransaction();
            // path old avatar
            $avatarPath = $user->avatar;
            if ($request->hasFile('avatar')) {
                if(Storage::disk('user')->exists($user->avatar)) {
                    Storage::disk('user')->delete($user->avatar);
                }
                $avatarPath = $this->uploadImage->uploadImageStorage($request->file('avatar'), "user");
            }
            $user->update([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'phone' => $request->phone,
                'department' => $request->department,
                'dateofbirth' => $request->dateofbirth,
                'role_id' => $request->role_id ? $request->role_id : config('common.role_user'),
                'avatar' => $avatarPath,
                'thumbnail_path' => $avatarPath,
            ]);
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
    public function changePassword(Request $request, $userId) {
        $this->validate($request, [
            'password_old' => 'required',
            'password_new' => 'different:password_old',
        ]);
        $user = User::find($userId);
        try {
            DB::beginTransaction();
            if (!Hash::check($request->password_old, $user->password)) {
                return response()->apiRetError(['Password Old Not Match!'], null, 400);
            }
            if (Hash::check($request->password_old, $user->password)) {
                $user->password = bcrypt($request->password_new);
                $user->save();
            }
            DB::commit();
            return response()->apiRet();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }

    }
    public function deleteUser($userId) {
        if (auth('api')->user()->role->name !== 'admin') {
            if (auth('api')->user()->role->name !== 'admin') {
                return response()->unauthorizedError();
            }
            return response()->internalError();
        }
        try {
            DB::beginTransaction();
            if(empty($userId)) {
                return response()->apiValidateError("User id cannot be empty");
            }
            $user = User::findOrFail($userId);
            $user->is_active = config('common.user_not_active');
            $user->save();
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
    public function search(Request $request) {
        try {
            $key = $request->key;
            $users = User::where('email', 'LIKE', "%{$key}%")->orWhere('name', 'LIKE', "%{$key}%")->orWhere('username', 'LIKE', "%{$key}%")->paginate(config('common.number_paginate'));
            $users->map(function ($item) {
                $item->roleName = $item->role->name;
                $item = User::createThumbnailUrl($item);
                return $item;
            });
            return response()->apiRet($users);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
}
