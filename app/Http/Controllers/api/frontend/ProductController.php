<?php

namespace App\Http\Controllers\api\frontend;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function getAllProducts($offset) {
        $productNew = Product::getListProducts($offset, config('common.paginate_product'));
        $productNew->map(function ($item) {
                $item = Product::createThumbnailUrl($item);
                return $item;
        });
        return response()->apiRet(['product_new' => $productNew]);
    }

    public function getOldProducts() {
        $productNew = Product::getOldProducts();
        $productNew->map(function ($item) {
            $item = Product::createThumbnailUrl($item);
            return $item;
        });
        return response()->apiRet($productNew);
    }

    public function getNewProducts() {
        $productNew = Product::getNewProducts();
        $productNew->map(function ($item) {
            $item = Product::createThumbnailUrl($item);
            return $item;
        });
        return response()->apiRet($productNew);
    }
    public function getNewDetailProducts() {
        $productNew = Product::orderBy('created_at', 'DESC')->limit(5)->get();
        $productNew->map(function ($item) {
            $item = Product::createThumbnailUrl($item);
            return $item;
        });
        return response()->apiRet($productNew);
    }
    public function getRelatedProducts($categoryId) {
        $products = Product::where('pro_category_id', $categoryId)->limit(5)->get();
        $products->map(function ($item) {
            $item = Product::createThumbnailUrl($item);
            return $item;
        });
        return response()->apiRet($products);
    }

    public function searchData() {
        $category = $_GET['category'];
        $key = $_GET['key'];
        $minPrice = $_GET['minPrice'];
        $maxPrice = $_GET['maxPrice'];

        dd($category);
    }

    public function getProductByCategory($categoryId) {
        $productByCate = Product::getProductByCategory($categoryId);
        $productByCate->map(function ($item) {
            $item = Product::createThumbnailUrl($item);
            return $item;
        });
        return response()->apiRet($productByCate);
    }


}
