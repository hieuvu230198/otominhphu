<?php

namespace App\Http\Controllers\api\frontend;

use App\Blog;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function getAllBlogs($offset) {
        $blogNew = Blog::getListBlogs($offset, config('common.paginate_product'));
        $blogNew->map(function ($item) {
            $item = Blog::createThumbnailUrl($item);
            return $item;
        });
        return response()->apiRet(['blog_new' => $blogNew]);
    }
    public function getBlogDetail($id) {
        $blogDetail = Blog::getBlogDetail($id);
        $blogDetail->thumbnail_path = Blog::createThumbnailUrl($blogDetail);
        return response()->apiRet($blogDetail);
    }
}
