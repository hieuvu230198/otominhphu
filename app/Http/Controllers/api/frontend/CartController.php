<?php

namespace App\Http\Controllers\api\frontend;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use stdClass;

class CartController extends Controller
{
    public function postCart(Request $request) {
        $order =  [];
        $customer = $request->customer;
        $carts = $request->carts;
        try {
            DB::beginTransaction();
            $order['or_cus_name'] = $customer['name'];
            $order['or_cus_email'] = $customer['email'];
            $order['or_cus_phone'] = $customer['phone'];
            $order['or_cus_city'] = $customer['country'] ? $customer['country'] : '';
            $order['or_cus_address'] = $customer['address'];
            $order['or_shipped_date'] = now();
            $order['or_pay_id'] = $customer['customer_payment'];
            $order['or_notes'] = $customer['description'];
            $order['or_total'] = (int)$request->total;
            $order['or_status_id'] = 0;
            $newOrderId = Order::create($order);
            $this->createOrderDetail($newOrderId->or_id, $carts);
            DB::commit();
            return response()->apiRet();
        }  catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->internalError($e->getMessage());
        }
    }
    public function createOrderDetail($orderId, $products) {
        $newOrderDetail = [];
        foreach ($products as $product) {
            $newOrderDetail['odetail_order_id'] = $orderId;
            $newOrderDetail['odetail_product_id'] = $product['id'];
            $newOrderDetail['odetail_unit_price'] = $product['price'];
            $newOrderDetail['odetail_quantity'] = $product['quantity'];
            $newOrderDetail['odetail_total_money'] = $product['price'] * $product['quantity'];
            DB::table('order_detail')->insert($newOrderDetail);
        }
    }
}
