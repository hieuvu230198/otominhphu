<?php

namespace App\Http\Controllers\api\frontend;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function getAllCategories() {
        $categories = Category::getAllCategories();
        return response()->apiRet($categories);
    }
}
