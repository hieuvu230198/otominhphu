<?php

return [
    'number_paginate' => 10,
    'role_user' => 2,
    'role_admin' => 1,
    'role_admin_name' => 'admin',
    'role_user_name' => 'user',
    'error_unauthorized' => 'Unauthorized',
    'user_active' => 1,
    'user_not_active' => 0,
    'active' => 1,
    'not_active' => 0,
    'paginate_product' => 12,
];
