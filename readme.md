#you may be need to run all of commands below to init the project
1. Create file .env
2. Run: composer install
3. Run: php artisan key:generate
4. Run: composer dump-autoload
5. Run: php artisan storage:link
6. Run: php artisan migrate:refresh --seed
7. Run: composer require phpoffice/phpexcel
8. Set APP_URL in .env file
    - Example
        - APP_URL= http://otominhphu.local
        - API_URL= http://otominhphu.local/api/v1
        - API_A UTH_URL= http://otominhphu.local/api/v1/auth
## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
